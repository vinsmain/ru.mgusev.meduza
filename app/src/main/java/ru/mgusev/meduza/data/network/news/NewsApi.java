package ru.mgusev.meduza.data.network.news;

import com.google.gson.JsonObject;

import java.util.Map;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface NewsApi {

    @GET("/api/w5/{url}")
    Single<Response<JsonObject>> getNewsDetails(@Path("url") String newsUrl);

    @GET("/api/w5/search")
    Single<Response<JsonObject>> getNewsList(@QueryMap Map<String, Object> params);
}
