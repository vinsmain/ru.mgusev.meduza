package ru.mgusev.meduza.data.network.news;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Collections;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import ru.mgusev.meduza.BuildConfig;
import ru.mgusev.meduza.data.dto.NewsDetailsApiResponse;
import ru.mgusev.meduza.data.dto.NewsListApiResponse;
import ru.mgusev.meduza.data.network._base.BaseNetwork;
import ru.mgusev.meduza.domain.dto.news.NewsList;
import ru.mgusev.meduza.domain.dto.news.details.NewsDetailsItem;

public class NewsNetwork extends BaseNetwork {

    private final NewsApi api;

    @Inject
    public NewsNetwork(Gson gson, NewsApi api) {
        super(gson);
        this.api = api;
    }

    public Single<NewsList> getNewsList(Map<String, Object> params) {
        return api.getNewsList(params)
                .flatMap(response -> parseResponse(response, new TypeToken<NewsListApiResponse>() {
                }))
                .map(NewsListApiResponse::getNewsList)
                .flatMap(newsList -> Observable.fromIterable(newsList.getNewsItemList())
                        .filter(newsItem -> newsItem.getUrl() != null && newsItem.getTitle() != null && (newsItem.getTag() == null || !newsItem.getTag().getName().equals("игры")))
                        .map(newsItem -> {
                            newsItem.setDateTime(newsItem.getDateTime() * 1000L);
                            return newsItem;
                        })
                        .toList()
                        .map(newsItemList -> {
                            Collections.sort(newsItemList, Collections.reverseOrder());
                            return newsItemList;
                        })
                        .map(newsItemList -> new NewsList(newsList.hasNext(), newsItemList)));
    }

    public Single<NewsDetailsItem> getNewsDetailsItem(String url) {
        return api.getNewsDetails(url)
                .flatMap(response -> parseResponse(response, new TypeToken<NewsDetailsApiResponse>() {
                }))
                .map(NewsDetailsApiResponse::getNewsDetailsItem)
                .map(newsDetailsItem -> {
                    newsDetailsItem.getContent().setBody("<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />" + newsDetailsItem.getContent().getBody().replace("src=\"/", "src=\"" + BuildConfig.SiteUrl + "/"));
                    newsDetailsItem.setDateTime(newsDetailsItem.getDateTime() * 1000L);
                    return newsDetailsItem;
                });
    }
}