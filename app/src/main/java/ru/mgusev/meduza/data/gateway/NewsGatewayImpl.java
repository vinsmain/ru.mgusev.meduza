package ru.mgusev.meduza.data.gateway;

import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.subjects.BehaviorSubject;
import ru.mgusev.meduza.data.local.database.AppDatabase;
import ru.mgusev.meduza.data.network.news.NewsNetwork;
import ru.mgusev.meduza.domain.dto.news.AudioState;
import ru.mgusev.meduza.domain.dto.news.NewsItem;
import ru.mgusev.meduza.domain.dto.news.NewsList;
import ru.mgusev.meduza.domain.dto.news.details.NewsDetailsItem;
import ru.mgusev.meduza.domain.gateway.NewsGateway;

public class NewsGatewayImpl implements NewsGateway {

    private final NewsNetwork network;
    private final AppDatabase database;
    private final BehaviorSubject<AudioState> audioStateSubject;
    private final BehaviorSubject<AudioState> audioProgressSubject;

    public NewsGatewayImpl(NewsNetwork network, AppDatabase database, BehaviorSubject<AudioState> audioStateSubject, BehaviorSubject<AudioState> audioProgressSubject) {
        this.network = network;
        this.database = database;
        this.audioStateSubject = audioStateSubject;
        this.audioProgressSubject = audioProgressSubject;
    }

    @Override
    public Single<NewsList> getNewsList(Map<String, Object> params) {
        return network.getNewsList(params);
    }

    @Override
    public Single<NewsDetailsItem> getNewsDetailsItem(String url) {
        return network.getNewsDetailsItem(url);
    }

    @Override
    public Flowable<List<NewsItem>> getBookmarkList() {
        return database.newsItemDao().getAll();
    }

    @Override
    public Flowable<List<NewsItem>> getBookmarkByUrl(String url) {
        return database.newsItemDao().getByUrl(url);
    }

    @Override
    public Completable insertBookmark(NewsItem bookmark) {
        return database.newsItemDao().insert(bookmark);
    }

    @Override
    public Completable deleteBookmark(NewsItem bookmark) {
        return database.newsItemDao().delete(bookmark);
    }

    @Override
    public BehaviorSubject<AudioState> getAudioStateSubject() {
        return audioStateSubject;
    }

    @Override
    public BehaviorSubject<AudioState> getAudioProgressSubject() {
        return audioProgressSubject;
    }
}