package ru.mgusev.meduza.data.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ru.mgusev.meduza.domain.dto.news.details.NewsDetailsItem;

public class NewsDetailsApiResponse implements Serializable {

    @SerializedName("root")
    private NewsDetailsItem newsDetailsItem;

    public NewsDetailsApiResponse() {
    }

    public NewsDetailsItem getNewsDetailsItem() {
        return newsDetailsItem;
    }

    public void setNewsDetailsItem(NewsDetailsItem newsDetailsItem) {
        this.newsDetailsItem = newsDetailsItem;
    }
}