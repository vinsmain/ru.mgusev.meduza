package ru.mgusev.meduza.data.local.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import ru.mgusev.meduza.domain.dto.news.NewsItem;

@Database(entities = {NewsItem.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract NewsItemDao newsItemDao();
}
