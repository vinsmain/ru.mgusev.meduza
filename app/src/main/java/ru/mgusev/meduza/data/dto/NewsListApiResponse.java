package ru.mgusev.meduza.data.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ru.mgusev.meduza.domain.dto.news.NewsItem;
import ru.mgusev.meduza.domain.dto.news.NewsList;


public class NewsListApiResponse implements Serializable {

    @SerializedName("has_next")
    private boolean hasNext;
    private Map<String, NewsItem> documents;
    private List<String> collections;
    @SerializedName("_count")
    private int count;

    public NewsListApiResponse() {
    }

    public boolean isHasNext() {
        return hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }

    public Map<String, NewsItem> getDocuments() {
        return documents;
    }

    public void setDocuments(Map<String, NewsItem> documents) {
        this.documents = documents;
    }

    public List<String> getCollections() {
        return collections;
    }

    public void setCollections(List<String> collections) {
        this.collections = collections;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public NewsList getNewsList() {
        return new NewsList(hasNext, new ArrayList<>(documents.values()));
    }
}