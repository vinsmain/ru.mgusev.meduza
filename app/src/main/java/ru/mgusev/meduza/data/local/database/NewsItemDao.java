package ru.mgusev.meduza.data.local.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import ru.mgusev.meduza.domain.dto.news.NewsItem;

@Dao
public interface NewsItemDao {

    @Query("SELECT * FROM news_items ORDER BY date_time DESC")
    Flowable<List<NewsItem>> getAll();

    @Query("SELECT * FROM news_items WHERE url = :url")
    Flowable<List<NewsItem>> getByUrl(String url);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insert(NewsItem newsItem);

    @Delete
    Completable delete(NewsItem newsItem);
}