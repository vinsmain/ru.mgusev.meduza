package ru.mgusev.meduza.domain.gateway;

import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.subjects.BehaviorSubject;
import ru.mgusev.meduza.domain.dto.news.AudioState;
import ru.mgusev.meduza.domain.dto.news.NewsItem;
import ru.mgusev.meduza.domain.dto.news.NewsList;
import ru.mgusev.meduza.domain.dto.news.details.NewsDetailsItem;

public interface NewsGateway {

    Single<NewsList> getNewsList(Map<String, Object> params);

    Single<NewsDetailsItem> getNewsDetailsItem(String url);

    Flowable<List<NewsItem>> getBookmarkList();

    Flowable<List<NewsItem>> getBookmarkByUrl(String url);

    Completable insertBookmark(NewsItem newsItem);

    Completable deleteBookmark(NewsItem newsItem);

    BehaviorSubject<AudioState> getAudioStateSubject();

    BehaviorSubject<AudioState> getAudioProgressSubject();
}
