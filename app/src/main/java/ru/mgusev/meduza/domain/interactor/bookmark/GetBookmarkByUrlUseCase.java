package ru.mgusev.meduza.domain.interactor.bookmark;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import ru.mgusev.meduza.domain.dto.news.NewsItem;
import ru.mgusev.meduza.domain.gateway.NewsGateway;
import ru.mgusev.meduza.domain.interactor._base.FlowableUseCase;
import ru.mgusev.meduza.domain.interactor._base.SchedulerProvider;

public class GetBookmarkByUrlUseCase extends FlowableUseCase<List<NewsItem>, GetBookmarkByUrlParams> {

    private final NewsGateway gateway;

    @Inject
    public GetBookmarkByUrlUseCase(SchedulerProvider schedulerProvider, NewsGateway gateway) {
        super(schedulerProvider);
        this.gateway = gateway;
    }

    @Override
    protected Flowable<List<NewsItem>> buildUseCaseFlowable(GetBookmarkByUrlParams params) {
        return gateway.getBookmarkByUrl(params.getBookmark().getUrl());
    }
}