package ru.mgusev.meduza.domain.interactor.bookmark;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import ru.mgusev.meduza.domain.interactor._base.BaseUseCaseParams;
import ru.mgusev.meduza.domain.dto.news.NewsItem;
import ru.mgusev.meduza.domain.gateway.NewsGateway;
import ru.mgusev.meduza.domain.interactor._base.FlowableUseCase;
import ru.mgusev.meduza.domain.interactor._base.SchedulerProvider;

public class GetBookmarkListUseCase extends FlowableUseCase<List<NewsItem>, BaseUseCaseParams> {

    private final NewsGateway gateway;

    @Inject
    public GetBookmarkListUseCase(SchedulerProvider schedulerProvider, NewsGateway gateway) {
        super(schedulerProvider);
        this.gateway = gateway;
    }

    @Override
    protected Flowable<List<NewsItem>> buildUseCaseFlowable(BaseUseCaseParams params) {
        return gateway.getBookmarkList();
    }
}