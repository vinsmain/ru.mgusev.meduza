package ru.mgusev.meduza.domain.interactor.bookmark;

import javax.inject.Inject;

import io.reactivex.Completable;
import ru.mgusev.meduza.domain.gateway.NewsGateway;
import ru.mgusev.meduza.domain.interactor._base.CompletableUseCase;
import ru.mgusev.meduza.domain.interactor._base.SchedulerProvider;

public class InsertBookmarkUseCase extends CompletableUseCase<InsertBookmarkParams> {

    private final NewsGateway gateway;

    @Inject
    public InsertBookmarkUseCase(SchedulerProvider schedulerProvider, NewsGateway gateway) {
        super(schedulerProvider);
        this.gateway = gateway;
    }

    @Override
    protected Completable buildUseCaseCompletable(InsertBookmarkParams params) {
        return gateway.insertBookmark(params.getBookmark());
    }
}