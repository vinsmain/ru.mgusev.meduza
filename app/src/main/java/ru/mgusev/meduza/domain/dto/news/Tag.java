package ru.mgusev.meduza.domain.dto.news;

import androidx.annotation.Nullable;

import java.io.Serializable;

public class Tag implements Serializable {

    private String path;
    private String name;

    public Tag() {
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Tag tag = (Tag) obj;
        return ((path == null && tag.getPath() == null) || path != null && tag.getPath() != null && path.equals(tag.getPath())) &&
                (name == null && tag.getName() == null) || (name != null && tag.getName() != null && name.equals(tag.getName()));
    }
}