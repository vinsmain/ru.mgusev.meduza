package ru.mgusev.meduza.domain.dto.news;

import androidx.annotation.Nullable;

import java.io.Serializable;

public class Source implements Serializable {

    private int trust;
    private String name;
    private String url;

    public Source() {
    }

    public int getTrust() {
        return trust;
    }

    public void setTrust(int trust) {
        this.trust = trust;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Source tag = (Source) obj;
        return ((url == null && tag.getUrl() == null) || url != null && tag.getUrl() != null && url.equals(tag.getUrl())) &&
                (name == null && tag.getName() == null) || (name != null && tag.getName() != null && name.equals(tag.getName()));
    }
}