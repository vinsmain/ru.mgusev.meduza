package ru.mgusev.meduza.domain.dto.news.details;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

import ru.mgusev.meduza.domain.dto.news.Audio;
import ru.mgusev.meduza.domain.dto.news.Image;
import ru.mgusev.meduza.domain.dto.news.Source;
import ru.mgusev.meduza.domain.dto.news.Tag;

public class NewsDetailsItem implements Serializable, Comparable<NewsDetailsItem> {

    private int version;
    private String url;
    private Content content;
    private String title;
    private Source source;
    @SerializedName("datetime")
    private long dateTime;
    private Tag tag;
    @SerializedName("second_title")
    private String secondTitle;
    private Image image;
    private Audio audio;
    private boolean bookmark;

    public NewsDetailsItem() {
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public String getSecondTitle() {
        return secondTitle;
    }

    public void setSecondTitle(String secondTitle) {
        this.secondTitle = secondTitle;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Audio getAudio() {
        return audio;
    }

    public void setAudio(Audio audio) {
        this.audio = audio;
    }

    public boolean isBookmark() {
        return bookmark;
    }

    public void setBookmark(boolean bookmark) {
        this.bookmark = bookmark;
    }

    @Override
    public int compareTo(@NotNull NewsDetailsItem newsDetailsItem) {
        return Long.compare(getDateTime(), newsDetailsItem.getDateTime());
    }
}