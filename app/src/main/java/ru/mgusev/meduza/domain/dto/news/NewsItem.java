package ru.mgusev.meduza.domain.dto.news;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

import ru.mgusev.meduza.domain.dto._base.Copyable;
import timber.log.Timber;

@Entity(tableName = "news_items")
public class NewsItem implements Serializable, Comparable<NewsItem> {

    public static final String KEY_TITLE = "title";
    public static final String KEY_TAG = "tag";
    public static final String KEY_DATE_TIME = "date_time";
    public static final String KEY_SECOND_TITLE = "second_title";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_BOOKMARK = "bookmark";
    public static final String KEY_PLAY = "play";

    @NonNull
    @PrimaryKey
    private String url;
    private int version;
    private String title;
    @Embedded(prefix = "tag")
    private Tag tag;
    @SerializedName("datetime")
    @ColumnInfo(name = "date_time")
    private long dateTime;
    @SerializedName("second_title")
    @ColumnInfo(name = "second_title")
    private String secondTitle;
    @Embedded(prefix = "image")
    private Image image;
    @Embedded(prefix = "audio")
    private Audio audio;
    private boolean bookmark;
    @Ignore
    private boolean play;

    public NewsItem() {
    }

    public NewsItem(NewsItem item) {
        this.url = item.getUrl();
        this.version = item.getVersion();
        this.title = item.getTitle();
        this.secondTitle = item.getSecondTitle();
        this.tag = item.getTag();
        this.dateTime = item.getDateTime();
        this.image = item.getImage();
        this.audio = item.getAudio();
        this.bookmark = item.isBookmark();
        this.play = item.isPlay();
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public String getSecondTitle() {
        return secondTitle;
    }

    public void setSecondTitle(String secondTitle) {
        this.secondTitle = secondTitle;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Audio getAudio() {
        return audio;
    }

    public void setAudio(Audio audio) {
        this.audio = audio;
    }

    public boolean isBookmark() {
        return bookmark;
    }

    public void setBookmark(boolean bookmark) {
        this.bookmark = bookmark;
    }

    public boolean isPlay() {
        return play;
    }

    public void setPlay(boolean play) {
        this.play = play;
    }

    public boolean equalsContent(NewsItem item) {
        return version == item.getVersion() &&
                title.equals(item.getTitle()) &&
                ((secondTitle == null && item.getSecondTitle() == null) || (secondTitle != null && item.getSecondTitle() != null && secondTitle.equals(item.getSecondTitle()))) &&
                ((tag == null && item.getTag() == null) || (tag != null && item.getTag() != null && tag.equals(item.getTag()))) &&
                dateTime == item.getDateTime() &&
                ((image == null && item.getImage() == null) || (image != null && item.getImage() != null && image.equals(item.getImage()))) &&
                bookmark == item.isBookmark() &&
                play == item.isPlay();
    }

    @Override
    public int compareTo(@NotNull NewsItem newsItem) {
        return Long.compare(getDateTime(), newsItem.getDateTime());
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        NewsItem item = (NewsItem) obj;
        return url.equals(item.getUrl());
    }
}