package ru.mgusev.meduza.domain.interactor.audio_player;

import javax.inject.Inject;

import io.reactivex.subjects.BehaviorSubject;
import ru.mgusev.meduza.domain.dto.news.AudioState;
import ru.mgusev.meduza.domain.gateway.NewsGateway;
import ru.mgusev.meduza.domain.interactor._base.BaseUseCaseParams;
import ru.mgusev.meduza.domain.interactor._base.BehaviorSubjectUseCase;
import ru.mgusev.meduza.domain.interactor._base.SchedulerProvider;

public class AudioPlayerProgressChangeUseCase extends BehaviorSubjectUseCase<AudioState, BaseUseCaseParams> {

    private final NewsGateway gateway;

    @Inject
    public AudioPlayerProgressChangeUseCase(SchedulerProvider schedulerProvider, NewsGateway gateway) {
        super(schedulerProvider);
        this.gateway = gateway;
    }

    @Override
    protected BehaviorSubject<AudioState> buildUseCaseBehaviorSubject(BaseUseCaseParams params) {
        return gateway.getAudioProgressSubject();
    }
}