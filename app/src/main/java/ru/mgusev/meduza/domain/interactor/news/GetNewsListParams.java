package ru.mgusev.meduza.domain.interactor.news;

import java.util.LinkedHashMap;
import java.util.Map;

import ru.mgusev.meduza.domain.interactor._base.BaseUseCaseParams;

public class GetNewsListParams extends BaseUseCaseParams {

    public static final int PER_PAGE = 24;

    private final String chrono;
    private final Integer page;
    private final Integer perPage;
    private final String locale;

    public GetNewsListParams(String chrono, Integer page) {
        this.chrono = chrono;
        this.page = page;
        this.perPage = PER_PAGE;
        this.locale = "ru";
    }

    public Map<String, Object> toParams() {
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("chrono", chrono);
        params.put("locale", locale);
        params.put("page", page);
        params.put("per_page", perPage);

        return params;
    }
}