package ru.mgusev.meduza.domain.dto.news;

import java.util.List;

public class NewsList {

    private boolean hasNext;
    private List<NewsItem> newsItemList;

    public NewsList(boolean hasNext, List<NewsItem> newsItemList) {
        this.hasNext = hasNext;
        this.newsItemList = newsItemList;
    }

    public boolean hasNext() {
        return hasNext;
    }

    public List<NewsItem> getNewsItemList() {
        return newsItemList;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }

    public void setNewsItemList(List<NewsItem> newsItemList) {
        this.newsItemList = newsItemList;
    }
}