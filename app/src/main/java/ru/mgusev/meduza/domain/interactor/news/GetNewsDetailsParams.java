package ru.mgusev.meduza.domain.interactor.news;

import ru.mgusev.meduza.domain.interactor._base.BaseUseCaseParams;

public class GetNewsDetailsParams extends BaseUseCaseParams {

    private String url;

    public GetNewsDetailsParams(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}