package ru.mgusev.meduza.domain.interactor.news;

import javax.inject.Inject;

import io.reactivex.Single;

import ru.mgusev.meduza.domain.dto.news.NewsList;
import ru.mgusev.meduza.domain.gateway.NewsGateway;
import ru.mgusev.meduza.domain.interactor._base.SchedulerProvider;
import ru.mgusev.meduza.domain.interactor._base.SingleUseCase;

public class GetNewsListUseCase extends SingleUseCase<NewsList, GetNewsListParams> {

    private final NewsGateway gateway;

    @Inject
    public GetNewsListUseCase(SchedulerProvider schedulerProvider, NewsGateway gateway) {
        super(schedulerProvider);
        this.gateway = gateway;
    }

    @Override
    protected Single<NewsList> buildUseCaseSingle(GetNewsListParams params) {
        return gateway.getNewsList(params.toParams());
    }
}
