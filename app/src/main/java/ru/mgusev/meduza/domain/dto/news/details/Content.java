package ru.mgusev.meduza.domain.dto.news.details;

import java.io.Serializable;
import java.util.List;

public class Content implements Serializable {

    private String body;
    private List<Episode> episodeList;

    public Content() {
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public List<Episode> getEpisodeList() {
        return episodeList;
    }

    public void setEpisodeList(List<Episode> episodeList) {
        this.episodeList = episodeList;
    }
}