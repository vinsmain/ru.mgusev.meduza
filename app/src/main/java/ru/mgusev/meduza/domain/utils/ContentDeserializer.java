package ru.mgusev.meduza.domain.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ru.mgusev.meduza.domain.dto.news.Audio;
import ru.mgusev.meduza.domain.dto.news.details.Content;
import ru.mgusev.meduza.domain.dto.news.details.Episode;
import timber.log.Timber;

public class ContentDeserializer implements JsonDeserializer<Content> {
    @Override
    public Content deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        Timber.d("1");
        JsonArray jsonArray = jsonObject.getAsJsonArray("blocks");
        Timber.d("2 " + jsonArray);
        if (jsonArray == null && jsonObject.getAsJsonArray("cards") != null) {
            jsonArray = new JsonArray();
            for (JsonElement jsonElement : jsonObject.getAsJsonArray("cards")) {
                jsonArray.addAll(jsonElement.getAsJsonObject().getAsJsonArray("blocks"));
            }
        }
        if (jsonArray == null && jsonObject.getAsJsonArray("slides") != null) {
            jsonArray = new JsonArray();
            for (JsonElement jsonElement : jsonObject.getAsJsonArray("slides")) {
                jsonArray.addAll(jsonElement.getAsJsonObject().getAsJsonArray("blocks"));
            }
        }
        Timber.d(String.valueOf(jsonArray));
        Timber.d(jsonArray.get(0).getAsJsonObject().getAsJsonPrimitive("type").getAsString());

        Content content = new Content();
        StringBuilder body = new StringBuilder();

        for (JsonElement element : jsonArray) {
            String type = element.getAsJsonObject().getAsJsonPrimitive("type").getAsString();
            Timber.d(type);

            switch (type) {
                case "card_title":
                    body.append("<hr><h3>");
                    body.append(element.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("text").getAsString());
                    body.append("</h3>");
                    break;
                case "lead":
                    body.append("<div class=\"Lead\"><p>");
                    body.append(element.getAsJsonObject().getAsJsonPrimitive("data").getAsString());
                    body.append("</p></div>");
                    break;
                case "lead_hr":
                    body.append("<hr>");
                    break;
                case "h1":
                case "h2":
                case "h3":
                    body.append("<h3>");
                    body.append(element.getAsJsonObject().getAsJsonPrimitive("data").getAsString());
                    body.append("</h3>");
                    break;
                case "h4":
                    body.append("<h4>");
                    body.append(element.getAsJsonObject().getAsJsonPrimitive("data").getAsString());
                    body.append("</h4>");
                    break;
                case "h5":
                    body.append("<h5>");
                    body.append(element.getAsJsonObject().getAsJsonPrimitive("data").getAsString());
                    body.append("</h5>");
                    break;
                case "h6":
                    body.append("<h6>");
                    body.append(element.getAsJsonObject().getAsJsonPrimitive("data").getAsString());
                    body.append("</h6>");
                    break;
                case "p":
                    body.append("<p>");
                    body.append(element.getAsJsonObject().getAsJsonPrimitive("data").getAsString());
                    body.append("</p>");
                    break;
                case "ul":
                    body.append("<ul>");
                    for (JsonElement li : element.getAsJsonObject().getAsJsonArray("data")) {
                        body.append("<li><span>");
                        body.append(li.getAsString());
                        body.append("</span></li>");
                    }
                    body.append("</ul>");
                    break;
                case "context_p":
                    body.append("<div class=\"Context-item\"><ul><li><span>");
                    body.append(element.getAsJsonObject().getAsJsonPrimitive("data").getAsString());
                    body.append("</span></li></ul></div>");
                    break;
                case "blockquote":
                    body.append("<blockquote>");
                    body.append(element.getAsJsonObject().getAsJsonPrimitive("data").getAsString());
                    body.append("</blockquote>");
                    break;
                case "embed":
                case "embed_code":
                    if (element.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("provider") == null || element.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("provider").getAsString().equals("youtube")) {
                        body.append("<figure class=\"Figure Figure--withEmbed Figure--fullWidth\">" +
                                "<div class=\"Embed-container\" style=\"width: 100%; height: 0px; position: relative; padding-bottom: 56.25%\">");
                        body.append(element.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("html").getAsString());
                        body.append("</div>");
                        body.append("<figcaption class=\"Figure-caption\">");

                        JsonPrimitive caption = element.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("caption");
                        body.append("<div class=\"Figure-title\">");
                        if (caption != null) body.append(caption.getAsString());
                        body.append("</div>");


                        JsonPrimitive credit = element.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("credit");
                        body.append("<div class=\"Figure-credits\">");
                        if (credit != null) body.append(credit.getAsString());
                        body.append("</div>");

                        body.append("</figcaption></figure>");
                    } else {
                        body.append("<div class=\"EmbedCode\">");
                        body.append(element.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("html").getAsString());
                        body.append("<div class=\"EmbedCode-meta\">");

                        JsonPrimitive captionEC = element.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("caption");
                        body.append("<div class=\"EmbedCode-caption\">");
                        if (captionEC != null) body.append(captionEC.getAsString());
                        body.append("</div>");

                        JsonPrimitive creditEC = element.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("credit");
                        body.append("<div class=\"EmbedCode-credit\">");
                        if (creditEC != null) body.append(creditEC.getAsString());
                        body.append("</div>");

                        body.append("</div></div>");
                    }
                    break;
                case "related":
                    body.append("<div class=\"Context\"><div class=\"Related\"><h3>");
                    body.append(element.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("title").getAsString());
                    body.append("</h3><ul>");
                    for (JsonElement jsonElement : element.getAsJsonObject().getAsJsonObject("data").getAsJsonArray("related")) {
                        body.append("<li><a class=\"Related--isRich\" href=\"");
                        String url = jsonElement.getAsJsonObject().getAsJsonPrimitive("url").getAsString();
                        body.append(url.contains("https://") || url.contains("http://") ? url : "https://meduza.io/" + url);
                        body.append("\"><span class=\"Related-title--first\">");
                        body.append(jsonElement.getAsJsonObject().getAsJsonPrimitive("title").getAsString());
                        body.append("<span> </span><span class=\"Related-title--second\">");
                        JsonPrimitive secondTitle = jsonElement.getAsJsonObject().getAsJsonPrimitive("second_title");
                        if (secondTitle != null) body.append(secondTitle.getAsString());
                        body.append("</span></a></li>");
                    }
                    body.append("</ul></div>");
                    break;
                case "source":
                    body.append("<blockquote class=\"SourceBlock-root\"><a href=\"");
                    body.append(element.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("url").getAsString());
                    body.append("\" target=\"_blank\" class=\"SourceQuote-link\"><p class=\"SourceBlock-text\">");
                    body.append(element.getAsJsonObject().getAsJsonObject("data").getAsJsonArray("quote").get(0).getAsJsonObject().getAsJsonPrimitive("data").getAsString());
                    body.append("</p><footer class=\"SourceQuote-footer\"><cite><span class=\"SourceQuote-arrow\"></span><span class=\"SourceQuote-origin\">");
                    body.append(element.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("origin").getAsString());
                    body.append("</span></cite></footer></a></blockquote>");
                    break;
                case "image":
                    body.append("<figure class=\"Figure\"><img src=\"");
                    body.append(element.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("large_url").getAsString());
                    body.append("\">");

                    JsonPrimitive captionImg = element.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("caption");
                    body.append("<figcaption class=\"Figure-caption\"><div class=\"Figure-title\">");
                    if (captionImg != null) body.append(captionImg.getAsString());
                    body.append("</div>");

                    JsonPrimitive creditImg = element.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("credit");
                    body.append("<div class=\"Figure-credits\">");
                    if (creditImg != null) body.append(creditImg.getAsString());
                    body.append("</div></figcaption></figure>");
                    break;
                case "episodes":
                    List<Episode> episodeList = new ArrayList<>();
                    for (JsonElement jsonElement : element.getAsJsonObject().getAsJsonArray("data")) {
                        Episode episode = new Episode();
                        episode.setTitle(jsonElement.getAsJsonObject().getAsJsonPrimitive("title").getAsString());
                        episode.setUrl(jsonElement.getAsJsonObject().getAsJsonPrimitive("url").getAsString());
                        episode.setSecondTitle(jsonElement.getAsJsonObject().getAsJsonPrimitive("subtitle").getAsString());
                        episode.setDateTime(jsonElement.getAsJsonObject().getAsJsonPrimitive("published_at").getAsLong());

                        Audio audio = new Audio();
                        audio.setUrl(jsonElement.getAsJsonObject().getAsJsonObject("audio").getAsJsonPrimitive("url").getAsString());
                        audio.setTitle(jsonElement.getAsJsonObject().getAsJsonObject("audio").getAsJsonPrimitive("title").getAsString());
                        audio.setSource(jsonElement.getAsJsonObject().getAsJsonObject("audio").getAsJsonPrimitive("mp3_url").getAsString());
                        audio.setDuration(jsonElement.getAsJsonObject().getAsJsonObject("audio").getAsJsonPrimitive("mp3_duration").getAsDouble());
                        audio.setDurationInWords(jsonElement.getAsJsonObject().getAsJsonObject("audio").getAsJsonPrimitive("mp3_duration_in_words").getAsString());
                        episode.setAudio(audio);

                        episodeList.add(episode);
                    }
                    content.setEpisodeList(episodeList);
                    break;
            }

        }
        content.setBody(body.toString());
        return content;
        //throw new JsonParseException("Wrong Content format");
    }
}