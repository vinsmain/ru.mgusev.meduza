package ru.mgusev.meduza.domain.dto.news.details;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ru.mgusev.meduza.domain.dto.news.Audio;

public class Episode implements Serializable {

    private String url;
    private String title;
    @SerializedName("published_at")
    private long dateTime;
    @SerializedName("subtitle")
    private String secondTitle;
    private Audio audio;

    public Episode() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public String getSecondTitle() {
        return secondTitle;
    }

    public void setSecondTitle(String secondTitle) {
        this.secondTitle = secondTitle;
    }

    public Audio getAudio() {
        return audio;
    }

    public void setAudio(Audio audio) {
        this.audio = audio;
    }
}