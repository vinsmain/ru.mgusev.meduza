package ru.mgusev.meduza.domain.dto.news;

public class AudioState {

    private Audio audio;
    private boolean playing;
    private long totalDuration;
    private long currentPosition;

    public AudioState(Audio audio, boolean playing, long totalDuration, long currentPosition) {
        this.audio = audio;
        this.playing = playing;
        this.totalDuration = totalDuration;
        this.currentPosition = currentPosition;
    }

    public Audio getAudio() {
        return audio;
    }

    public void setAudio(Audio audio) {
        this.audio = audio;
    }

    public boolean isPlaying() {
        return playing;
    }

    public void setPlaying(boolean playing) {
        this.playing = playing;
    }

    public long getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(long totalDuration) {
        this.totalDuration = totalDuration;
    }

    public long getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(long currentPosition) {
        this.currentPosition = currentPosition;
    }
}