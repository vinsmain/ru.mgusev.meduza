package ru.mgusev.meduza.domain.dto.news;

import java.io.Serializable;

public class Category implements Serializable {

    private String id;
    private String name;

    public Category(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Category(String name) {
        //this.id = UUID.randomUUID().toString();
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}