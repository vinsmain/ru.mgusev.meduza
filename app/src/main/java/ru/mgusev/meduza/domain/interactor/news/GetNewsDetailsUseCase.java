package ru.mgusev.meduza.domain.interactor.news;

import javax.inject.Inject;

import io.reactivex.Single;
import ru.mgusev.meduza.domain.dto.news.details.NewsDetailsItem;
import ru.mgusev.meduza.domain.gateway.NewsGateway;
import ru.mgusev.meduza.domain.interactor._base.SchedulerProvider;
import ru.mgusev.meduza.domain.interactor._base.SingleUseCase;

public class GetNewsDetailsUseCase extends SingleUseCase<NewsDetailsItem, GetNewsDetailsParams> {

    private final NewsGateway gateway;

    @Inject
    public GetNewsDetailsUseCase(SchedulerProvider schedulerProvider, NewsGateway gateway) {
        super(schedulerProvider);
        this.gateway = gateway;
    }

    @Override
    protected Single<NewsDetailsItem> buildUseCaseSingle(GetNewsDetailsParams params) {
        return gateway.getNewsDetailsItem(params.getUrl());
    }
}
