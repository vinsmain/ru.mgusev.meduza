package ru.mgusev.meduza.domain.dto.news;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ru.mgusev.meduza.BuildConfig;

public class Image implements Serializable {

    @SerializedName("isMobile")
    private String url; //for news
    @SerializedName("small_url")
    private String smallUrl; //for podcasts
    private String caption;
    private String credit;

    public Image() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSmallUrl() {
        return smallUrl;
    }

    public void setSmallUrl(String smallUrl) {
        this.smallUrl = smallUrl;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getAbsoluteImageUrl() {
        if (smallUrl != null && !smallUrl.equals(""))
            return BuildConfig.SiteUrl + smallUrl;
        else
            return BuildConfig.SiteUrl + url;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Image image = (Image) obj;
        return (getUrl() == null && image.getUrl() == null) || (getUrl() != null && image.getUrl() != null && getUrl().equals(image.getUrl())) &&
                (caption == null && image.getCaption() == null) || (caption != null && image.getCaption() != null && caption.equals(image.getCaption())) &&
                (credit == null && image.getCredit() == null) || (credit != null && image.getCredit() != null && credit.equals(image.getCredit()));
    }
}