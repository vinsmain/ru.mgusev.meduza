package ru.mgusev.meduza.domain.dto.news;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ru.mgusev.meduza.BuildConfig;

public class Audio implements Serializable {

    private String url;
    private String title;
    @SerializedName("mp3_url")
    private String source;
    @SerializedName("mp3_duration_in_words")
    private String durationInWords;
    @SerializedName("mp3_duration")
    private double duration;
    @SerializedName("cover_url")
    private String coverUrl;

    public Audio() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDurationInWords() {
        return durationInWords;
    }

    public void setDurationInWords(String durationInWords) {
        this.durationInWords = durationInWords;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getAbsoluteAudioUrl() {
        return BuildConfig.SiteUrl + getSource();
    }

    public String getAbsoluteNewsUrl() {
        return BuildConfig.SiteUrl + "/" + getUrl();
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Audio audio = (Audio) obj;

        return (url == null && audio.getUrl() == null) || (url != null && audio.getUrl() != null && url.equals(audio.getUrl())) &&
                (getSource() == null && audio.getSource() == null) || (getSource() != null && audio.getSource() != null && getSource().equals(audio.getSource()));
    }
}