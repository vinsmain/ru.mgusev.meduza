package ru.mgusev.meduza.domain.dto._base;

public interface Copyable<T> {

    T copy();
}
