package ru.mgusev.meduza.domain.interactor.bookmark;

import javax.inject.Inject;

import io.reactivex.Completable;
import ru.mgusev.meduza.domain.gateway.NewsGateway;
import ru.mgusev.meduza.domain.interactor._base.CompletableUseCase;
import ru.mgusev.meduza.domain.interactor._base.SchedulerProvider;

public class DeleteBookmarkUseCase extends CompletableUseCase<DeleteBookmarkParams> {

    private final NewsGateway gateway;

    @Inject
    public DeleteBookmarkUseCase(SchedulerProvider schedulerProvider, NewsGateway gateway) {
        super(schedulerProvider);
        this.gateway = gateway;
    }

    @Override
    protected Completable buildUseCaseCompletable(DeleteBookmarkParams params) {
        return gateway.deleteBookmark(params.getBookmark());
    }
}