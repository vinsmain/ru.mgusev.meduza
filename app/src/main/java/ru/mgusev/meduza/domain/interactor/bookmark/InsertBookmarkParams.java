package ru.mgusev.meduza.domain.interactor.bookmark;

import ru.mgusev.meduza.domain.interactor._base.BaseUseCaseParams;
import ru.mgusev.meduza.domain.dto.news.NewsItem;

public class InsertBookmarkParams extends BaseUseCaseParams {

    private NewsItem newsItem;

    public InsertBookmarkParams(NewsItem newsItem) {
        this.newsItem = newsItem;
    }

    public NewsItem getBookmark() {
        return newsItem;
    }
}