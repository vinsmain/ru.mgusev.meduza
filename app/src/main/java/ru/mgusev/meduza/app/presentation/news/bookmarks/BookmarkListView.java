package ru.mgusev.meduza.app.presentation.news.bookmarks;

import java.util.List;

import moxy.viewstate.strategy.OneExecutionStateStrategy;
import moxy.viewstate.strategy.StateStrategyType;
import ru.mgusev.meduza.app.presentation.base.BaseView;
import ru.mgusev.meduza.domain.dto.news.Audio;
import ru.mgusev.meduza.domain.dto.news.NewsItem;

public interface BookmarkListView extends BaseView {
    void showNewsList(List<NewsItem> newsList);

    void invalidateItem(NewsItem newsItem);

    void enableLoadMore();

    void disableLoadMore();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void onRefreshAdapter();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void updateAudioPlayerState(Audio audio);
}