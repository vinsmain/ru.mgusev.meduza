package ru.mgusev.meduza.app.presentation.news.pager;

import java.util.List;

import moxy.viewstate.strategy.AddToEndSingleStrategy;
import moxy.viewstate.strategy.AddToEndSingleTagStrategy;
import moxy.viewstate.strategy.OneExecutionStateStrategy;
import moxy.viewstate.strategy.StateStrategyType;
import ru.mgusev.meduza.app.presentation.base.BaseView;
import ru.mgusev.meduza.domain.dto.news.NewsItem;

public interface NewsPagerView extends BaseView {

//    @StateStrategyType(AddToEndSingleStrategy.class)
//    void showNewsList(List<NewsItem> newsItemList);
//
//    @StateStrategyType(OneExecutionStateStrategy.class)
//    void invalidateItem(NewsItem newsItem);
//
//    @StateStrategyType(value = AddToEndSingleTagStrategy.class, tag = "loadMore")
//    void enableLoadMore();
//
//    @StateStrategyType(value = AddToEndSingleTagStrategy.class, tag = "loadMore")
//    void disableLoadMore();
//
//    @StateStrategyType(OneExecutionStateStrategy.class)
//    void onRefreshAdapter();
}
