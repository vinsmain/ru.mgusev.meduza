package ru.mgusev.meduza.app.ui.news.inteface;

public interface OnBookmarkIconClick<T> {
    void onBookmarkIconClick(T item);
}