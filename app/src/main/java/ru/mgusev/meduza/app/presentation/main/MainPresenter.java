package ru.mgusev.meduza.app.presentation.main;

import moxy.InjectViewState;
import ru.mgusev.meduza.app.presentation.base.BasePresenter;
import ru.mgusev.meduza.app.presentation.base.ResourceManager;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class MainPresenter extends BasePresenter<MainView> {

    public MainPresenter(Router router, ResourceManager resourceManager) {
        super(router, resourceManager);
    }

    @Override
    protected void onFirstViewAttach() {
        getViewState().setNavigationItemSelected(0);
    }
}
