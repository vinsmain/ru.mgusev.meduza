package ru.mgusev.meduza.app.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.mgusev.meduza.app.di.ApplicationFragmentBindingModule;
import ru.mgusev.meduza.app.ui.ApplicationActivity;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = {ApplicationFragmentBindingModule.class})
    abstract ApplicationActivity bindApplication();
}
