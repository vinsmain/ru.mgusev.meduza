package ru.mgusev.meduza.app.ui.news.news_list;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.View;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import moxy.presenter.InjectPresenter;
import moxy.presenter.ProvidePresenter;
import ru.mgusev.meduza.R;
import ru.mgusev.meduza.app.navigation.RouterProvider;
import ru.mgusev.meduza.app.presentation.base.ResourceManager;
import ru.mgusev.meduza.app.presentation.news.news_list.NewsListPresenter;
import ru.mgusev.meduza.app.presentation.news.news_list.NewsListView;
import ru.mgusev.meduza.app.ui.ApplicationActivity;
import ru.mgusev.meduza.app.ui._base.BaseFragment;
import ru.mgusev.meduza.app.ui._base.Layout;
import ru.mgusev.meduza.app.ui._base.recycler.EndlessRecyclerViewScrollListener;
import ru.mgusev.meduza.app.ui.news.list_adapter.NewsAdapter;
import ru.mgusev.meduza.domain.dto.news.Audio;
import ru.mgusev.meduza.domain.dto.news.Category;
import ru.mgusev.meduza.domain.dto.news.NewsItem;
import ru.mgusev.meduza.domain.interactor.audio_player.AudioPlayerStateChangeUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.DeleteBookmarkUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.GetBookmarkListUseCase;
import ru.mgusev.meduza.domain.interactor.news.GetNewsListUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.InsertBookmarkUseCase;
import timber.log.Timber;

@Layout(id = R.layout.fragment_list)
public class NewsListFragment extends BaseFragment implements NewsListView {

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Inject
    ResourceManager resourceManager;
    @Inject
    GetNewsListUseCase getNewsListUseCase;
    @Inject
    GetBookmarkListUseCase getBookmarkListUseCase;
    @Inject
    InsertBookmarkUseCase insertBookmarkUseCase;
    @Inject
    DeleteBookmarkUseCase deleteBookmarkUseCase;
    @Inject
    AudioPlayerStateChangeUseCase audioPlayerStateChangeUseCase;

    private EndlessRecyclerViewScrollListener scrollListener;

    @InjectPresenter
    NewsListPresenter presenter;

    @ProvidePresenter
    NewsListPresenter providePresenter() {
        Timber.d("providePresenter");
        return new NewsListPresenter(
                ((RouterProvider) getActivity()).getRouter(),
                resourceManager,
                getCategory(),
                getNewsListUseCase,
                getBookmarkListUseCase,
                insertBookmarkUseCase,
                deleteBookmarkUseCase,
                audioPlayerStateChangeUseCase
        );
    }

    public static NewsListFragment getInstance(Category category) {
        Bundle args = new Bundle();
        args.putSerializable("category", category);

        NewsListFragment instance = new NewsListFragment();
        instance.setArguments(args);
        return instance;
    }

    private Category getCategory() {
        if (getArguments() != null && getArguments().containsKey("category")) {
            return (Category) getArguments().getSerializable("category");
        }

        return null;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.onRefresh());
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1));

        recyclerView.setAdapter(new NewsAdapter(
                getString(R.string.error_connection_timeout),
                getString(R.string.adapter_base_text_no_data)));
        ((NewsAdapter) recyclerView.getAdapter()).setItemClick(this::onItemClick);
        ((NewsAdapter) recyclerView.getAdapter()).setBookmarkIconClick(this::onBookmarkIconClick);
        ((NewsAdapter) recyclerView.getAdapter()).setPlayerControlBtnClick(this::onPlayerControlBtnClick);
        ((NewsAdapter) recyclerView.getAdapter()).setErrorListener(() -> presenter.onLoadMore());

        scrollListener = new EndlessRecyclerViewScrollListener(recyclerView.getLayoutManager(), presenter::onLoadMore);
    }

    @Override
    public void showNewsList(List<NewsItem> newsList) {
        ((NewsAdapter) recyclerView.getAdapter()).setItems(newsList);
    }

    @Override
    public void invalidateItem(NewsItem newsItem) {
        ((NewsAdapter) recyclerView.getAdapter()).invalidateItem(newsItem);
    }

    @Override
    public void enableLoadMore() {
        recyclerView.addOnScrollListener(scrollListener);
    }

    @Override
    public void disableLoadMore() {
        recyclerView.removeOnScrollListener(scrollListener);
    }

    @Override
    public void onRefreshAdapter() {
        disableLoadMore();
        ((NewsAdapter) recyclerView.getAdapter()).refresh();
        swipeRefreshLayout.setRefreshing(false);
    }

    private void onItemClick(NewsItem newsItem) {
        presenter.onItemClicked(newsItem);
    }

    private void onBookmarkIconClick(NewsItem newsItem) {
        Timber.d(newsItem + " " + newsItem.isBookmark());
        presenter.onBookmarkIconClicked(newsItem);
    }

    private void onPlayerControlBtnClick(NewsItem newsItem) {
        Timber.d(newsItem + " " + newsItem.isPlay());
        presenter.onAudioControlButtonClicked(newsItem);
    }

    @Override
    public void updateAudioPlayerState(Audio audio){
        ApplicationActivity activity = ((ApplicationActivity) getActivity());
        if (activity != null) {
            activity.updateAudioPlayerState(audio);
        }
    }
}