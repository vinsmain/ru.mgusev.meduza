package ru.mgusev.meduza.app.ui.news.inteface;

public interface OnPlayerControlBtnClick<T> {
    void onPlayerControlBtnClick(T item);
}