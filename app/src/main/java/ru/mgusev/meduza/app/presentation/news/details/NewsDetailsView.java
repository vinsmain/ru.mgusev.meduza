package ru.mgusev.meduza.app.presentation.news.details;

import moxy.viewstate.strategy.OneExecutionStateStrategy;
import moxy.viewstate.strategy.StateStrategyType;
import ru.mgusev.meduza.app.presentation.base.BaseView;
import ru.mgusev.meduza.domain.dto.news.Audio;

public interface NewsDetailsView extends BaseView {

    void setNewsDetailsItem(String body);

    void setTitle(String title);

    void setSource(String source);

    void setDateTime(long dateTime);

    void setTag(String tag);

    void showContent();

    void showLoader();

    void showError();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void updateAudioPlayerState(Audio audio);

    void setVisibilityAudioPlayer(boolean visible);

    void updateDuration(int total, int current);

    void changeAudioControlButtonIcon(boolean playing);

    void changeBookmarkIcon(boolean bookmark);
}
