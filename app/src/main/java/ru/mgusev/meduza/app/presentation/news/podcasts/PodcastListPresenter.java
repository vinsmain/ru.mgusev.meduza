package ru.mgusev.meduza.app.presentation.news.podcasts;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.subscribers.DisposableSubscriber;
import moxy.InjectViewState;
import ru.mgusev.meduza.app.navigation.Screens;
import ru.mgusev.meduza.app.presentation.base.BasePresenter;
import ru.mgusev.meduza.app.presentation.base.ResourceManager;
import ru.mgusev.meduza.domain.dto.news.AudioState;
import ru.mgusev.meduza.domain.dto.news.Category;
import ru.mgusev.meduza.domain.dto.news.NewsItem;
import ru.mgusev.meduza.domain.dto.news.NewsList;
import ru.mgusev.meduza.domain.interactor._base.BaseUseCaseParams;
import ru.mgusev.meduza.domain.interactor.audio_player.AudioPlayerStateChangeUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.DeleteBookmarkParams;
import ru.mgusev.meduza.domain.interactor.bookmark.DeleteBookmarkUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.GetBookmarkListUseCase;
import ru.mgusev.meduza.domain.interactor.news.GetNewsListParams;
import ru.mgusev.meduza.domain.interactor.news.GetNewsListUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.InsertBookmarkParams;
import ru.mgusev.meduza.domain.interactor.bookmark.InsertBookmarkUseCase;
import ru.terrakok.cicerone.Router;
import timber.log.Timber;

@InjectViewState
public class PodcastListPresenter extends BasePresenter<PodcastListView> {

    private final Category category;
    private final GetNewsListUseCase getNewsListUseCase;
    private final GetBookmarkListUseCase getBookmarkListUseCase;
    private final InsertBookmarkUseCase insertBookmarkUseCase;
    private final DeleteBookmarkUseCase deleteBookmarkUseCase;
    private final AudioPlayerStateChangeUseCase audioPlayerStateChangeUseCase;

    private List<NewsItem> newsItemList;
    private int page = 0;

    public PodcastListPresenter(Router router,
                                ResourceManager resourceManager,
                                Category category,
                                GetNewsListUseCase getNewsListUseCase,
                                GetBookmarkListUseCase getBookmarkListUseCase,
                                InsertBookmarkUseCase insertBookmarkUseCase,
                                DeleteBookmarkUseCase deleteBookmarkUseCase,
                                AudioPlayerStateChangeUseCase audioPlayerStateChangeUseCase) {

        super(router, resourceManager);

        this.category = category;
        this.getNewsListUseCase = getNewsListUseCase;
        this.getBookmarkListUseCase = getBookmarkListUseCase;
        this.insertBookmarkUseCase = insertBookmarkUseCase;
        this.deleteBookmarkUseCase = deleteBookmarkUseCase;
        this.audioPlayerStateChangeUseCase = audioPlayerStateChangeUseCase;

        this.newsItemList = new ArrayList<>();

        addUseCase(this.getNewsListUseCase);
        addUseCase(this.getBookmarkListUseCase);
        addUseCase(this.insertBookmarkUseCase);
        addUseCase(this.deleteBookmarkUseCase);
        addUseCase(this.audioPlayerStateChangeUseCase);
    }

    @Override
    protected void onFirstViewAttach() {
        loadData();
    }

    private void initBookmarkSubscribe() {
        getBookmarkListUseCase.execute(new DisposableSubscriber<List<NewsItem>>() {
            @Override
            public void onNext(List<NewsItem> bookmarks) {
                if (newsItemList.size() > 0) {
                    for (NewsItem item : newsItemList) {
                        item.setBookmark(bookmarks.contains(item));
                    }
                    getViewState().showNewsList(newsItemList);
                }
            }

            @Override
            public void onError(Throwable t) {
                Timber.d(t);
            }

            @Override
            public void onComplete() {
                Timber.d("Complete");
            }
        });
    }

    private void loadData() {
        getViewState().disableLoadMore();
        getNewsListUseCase.execute(new DisposableSingleObserver<NewsList>() {
            @Override
            public void onSuccess(NewsList newsList) {
                for (NewsItem newsItem : newsList.getNewsItemList()) {
                    if (!newsItemList.contains(newsItem))
                        newsItemList.add(newsItem);
                }
                initBookmarkSubscribe();
                initAudioPlayerStateSubscribe();

                if (newsList.getNewsItemList().size() == 0 && newsList.hasNext()) {
                    onLoadMore();
                }

                if (newsList.hasNext()) {
                    getViewState().enableLoadMore();
                }
            }

            @Override
            public void onError(Throwable e) {
                getViewState().showNewsList(null);
                Timber.d(e);
            }
        }, new GetNewsListParams(category.getName(), page++));
    }

    public void onLoadMore() {
        loadData();
    }

    private void initAudioPlayerStateSubscribe() {
        audioPlayerStateChangeUseCase.execute(new DisposableObserver<AudioState>() {

            @Override
            public void onNext(AudioState state) {
                for (NewsItem item : newsItemList) {
                    if (item.getUrl().equals(state.getAudio().getUrl())) {
                        item.setPlay(state.isPlaying());
                    } else {
                        item.setPlay(false);
                    }
                }
                getViewState().showNewsList(newsItemList);
            }

            @Override
            public void onError(Throwable e) {
                Timber.d(e);
            }

            @Override
            public void onComplete() {
            }
        }, new BaseUseCaseParams());
    }

    public void onRefresh() {
        page = 0;
        newsItemList.clear();
        getViewState().onRefreshAdapter();
        loadData();
    }

    public void onItemClicked(NewsItem newsItem) {
        router.navigateTo(new Screens.NewsDetailsScreen(newsItem.getUrl()));
    }

    public void onBookmarkIconClicked(NewsItem newsItem) {
        NewsItem item = newsItemList.get(newsItemList.indexOf(newsItem));

        if (item.isBookmark()) {
            deleteBookmarkUseCase.execute(new DeleteBookmarkParams(item));
        } else {
            item.setBookmark(true);
            insertBookmarkUseCase.execute(new InsertBookmarkParams(item));
        }
    }

    public void onAudioControlButtonClicked(NewsItem newsItem) {
        getViewState().updateAudioPlayerState(newsItem.getAudio());
    }
}