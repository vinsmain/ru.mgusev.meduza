package ru.mgusev.meduza.app;

import com.github.anrwatchdog.ANRWatchDog;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import ru.mgusev.meduza.BuildConfig;
import ru.mgusev.meduza.app.di.ApplicationComponent;
import ru.mgusev.meduza.app.di.DaggerApplicationComponent;
import ru.mgusev.meduza.domain.utils.LogDebugTree;
import timber.log.Timber;


/**
 * Тестовое задание
 * Разработать мобильное приложение - Новостная лента Meduza.io
 *
 * Входные данные:
 * У медузы есть два метода api
 * 1. https://meduza.io/api/w4/search - получение списка новостей
 * Принимает GET параметры:
 * chrono — принимает значения news, cards, articles, shapito или polygon, в зависимости от рубрики, которую хотим получить;
 * page — номер страницы;
 * per_page — количество записей на странице;
 * locale — локаль ru или en;
 * 2. https://meduza.io/api/w4/{url} - для получения подробной новости
 * где {url} - url новости полученный при использовании первого метода
 *
 * Что нужно реализовать
 * - Список новостей
 * - Просмотр детальной новости
 * - Фильтрацию по рубрикам(chrono из метода api)
 * - Вкладку избранное(локально сохранять что та или иная статья находится в избранном)
 *
 * Дополнительно можно сделать:
 * - Сплеш скрин(погубил какой вариант считается наилучшим и как его сделать)
 * - Дизайн на твое усмотрение, но нужно следовать гайдам Material Design
 * - Еще можно прикрутить кеширование ленты новостей, на случай если нет интернета в тот момент когда ты запустил снова приложение
 *
 * API https://pypi.org/project/meduza/
 */

public class Application extends DaggerApplication {
    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        ApplicationComponent component = DaggerApplicationComponent.builder().application(this).build();
        component.inject(this);

        if (BuildConfig.DEBUG) {
            Timber.plant(new LogDebugTree());
        }

        return component;
    }
}
