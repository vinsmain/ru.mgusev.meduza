package ru.mgusev.meduza.app.ui.news.list_adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import ru.mgusev.meduza.R;
import ru.mgusev.meduza.app.ui._base.recycler.adapter.BaseAdapter;
import ru.mgusev.meduza.app.ui.news.inteface.OnBookmarkIconClick;
import ru.mgusev.meduza.app.ui.news.inteface.OnPlayerControlBtnClick;
import ru.mgusev.meduza.domain.dto.news.NewsItem;
import timber.log.Timber;

public class NewsAdapter extends BaseAdapter<NewsViewHolder, NewsItem> implements OnBookmarkIconClick<NewsItem>, OnPlayerControlBtnClick<NewsItem> {

    private OnBookmarkIconClick<NewsItem> bookmarkIconClick;
    private OnPlayerControlBtnClick<NewsItem> playerControlBtnClick;

    public NewsAdapter(String errorString, String noDataString) {
        super(errorString, noDataString);
    }

    @Override
    public DiffUtilCallback<NewsItem> getDiffUtils(List<NewsItem> oldItems, List<NewsItem> newItems) {
        return new DiffUtilCallback<NewsItem>(oldItems, newItems) {
            @Override
            public boolean areItemsTheSame(int i, int i1) {
                NewsItem oldItem = oldItems.get(i);
                NewsItem newItem = newItems.get(i1);

                return oldItem != null && newItem != null && oldItem.getUrl().equals(newItem.getUrl());
            }

            @Override
            public boolean areContentsTheSame(int i, int i1) {
                NewsItem oldItem = oldItems.get(i);
                NewsItem newItem = newItems.get(i1);

                return  oldItem.equalsContent(newItem);
            }

            @Nullable
            @Override
            public Object getChangePayload(int i, int i1) {
                NewsItem oldItem = oldItems.get(i);
                NewsItem newItem = newItems.get(i1);

                Bundle diffBundle = new Bundle();

                if (oldItem.getVersion() != newItem.getVersion()) {
                    return null;
                }
                if (!oldItem.getTitle().equals(newItem.getTitle())) {
                    diffBundle.putString(NewsItem.KEY_TITLE, newItem.getTitle());
                }
                if (oldItem.getDateTime() != newItem.getDateTime()) {
                    diffBundle.putLong(NewsItem.KEY_DATE_TIME, newItem.getDateTime());
                }
                if (oldItem.getSecondTitle() != null && newItem.getSecondTitle() != null && oldItem.getSecondTitle().equals(newItem.getSecondTitle())) {
                    diffBundle.putString(NewsItem.KEY_SECOND_TITLE, newItem.getSecondTitle());
                }
                if (oldItem.getTag() != null && newItem.getTag() != null && oldItem.getTag().equals(newItem.getTag())) {
                    diffBundle.putSerializable(NewsItem.KEY_TAG, newItem.getTag());
                }
                if (oldItem.getImage() != null && newItem.getImage() != null && oldItem.getImage().equals(newItem.getImage())) {
                    diffBundle.putSerializable(NewsItem.KEY_IMAGE, newItem.getImage());
                }
                if (oldItem.isBookmark() != newItem.isBookmark()) {
                    diffBundle.putBoolean(NewsItem.KEY_BOOKMARK, newItem.isBookmark());
                }
                if (oldItem.isPlay() != newItem.isPlay()) {
                    diffBundle.putBoolean(NewsItem.KEY_PLAY, newItem.isPlay());
                }
                if (diffBundle.size() == 0) return null;
                return diffBundle;
            }
        };
    }

    @Override
    public NewsViewHolder getItemViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NewsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_news_list_item, parent, false), itemClick, bookmarkIconClick, playerControlBtnClick);
    }

    public void invalidateItem(NewsItem item) {
        int position = items.indexOf(item);
        notifyItemChanged(position);
    }

    public void setBookmarkIconClick(OnBookmarkIconClick<NewsItem> bookmarkIconClick) {
        this.bookmarkIconClick = bookmarkIconClick;
    }

    public void setPlayerControlBtnClick(OnPlayerControlBtnClick<NewsItem> playerControlBtnClick) {
        Timber.d(String.valueOf(playerControlBtnClick));
        this.playerControlBtnClick = playerControlBtnClick;
    }

    @Override
    public void onBookmarkIconClick(NewsItem item) {
        bookmarkIconClick.onBookmarkIconClick(item);
    }

    @Override
    public void onPlayerControlBtnClick(NewsItem item) {
        Timber.d("onClick");
        playerControlBtnClick.onPlayerControlBtnClick(item);
    }
}