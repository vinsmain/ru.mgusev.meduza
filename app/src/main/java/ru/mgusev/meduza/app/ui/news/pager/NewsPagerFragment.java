package ru.mgusev.meduza.app.ui.news.pager;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import moxy.presenter.InjectPresenter;
import moxy.presenter.ProvidePresenter;
import ru.mgusev.meduza.R;
import ru.mgusev.meduza.app.presentation.base.ResourceManager;
import ru.mgusev.meduza.app.presentation.news.pager.NewsPagerPresenter;
import ru.mgusev.meduza.app.presentation.news.pager.NewsPagerView;
import ru.mgusev.meduza.app.ui._base.BaseFragment;
import ru.mgusev.meduza.app.ui._base.BaseTabContainerFragment;
import ru.mgusev.meduza.app.ui._base.Layout;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.Router;

@Layout(id = R.layout.fragment_news_pager)
public class NewsPagerFragment extends BaseFragment implements NewsPagerView {

    @BindView(R.id.newsTabLayout)
    TabLayout newsTabLayout;

    @BindView(R.id.newsPager)
    ViewPager newsPager;

    @Inject
    ResourceManager resourceManager;

    @InjectPresenter
    NewsPagerPresenter presenter;

    @Inject
    Router router;

    @ProvidePresenter
    NewsPagerPresenter providePresenter() {
        return new NewsPagerPresenter(router, resourceManager);
    }

    public static NewsPagerFragment getInstance() {
        return new NewsPagerFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getChildFragmentManager().findFragmentById(R.id.fragmentContainer) == null) {

        } else {
            //onTabSelected(currentTab);
        }
        //bottomNavigationView.setOnNavigationItemSelectedListener(this);
        NewsPagerAdapter newsPagerAdapter = new NewsPagerAdapter(Objects.requireNonNull(getContext()), getChildFragmentManager());

        newsPager.setAdapter(newsPagerAdapter);

        newsTabLayout.setupWithViewPager(newsPager);
    }

//    @Override
//    public void setFragment() {
//        FragmentManager fragmentManager = getFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//
//        fragmentTransaction.replace(R.id.mainFragmentContainer, NewsPagerFragment.getInstance());
//        fragmentTransaction.addToBackStack(null);
//
//// Commit the transaction
//        fragmentTransaction.commit();
//    }


//    @Override
//    public boolean onBackPressed() {
////        FragmentManager fm = getChildFragmentManager();
////        Fragment fragment = null;
////        List<Fragment> fragments = fm.getFragments();
////        if (fragments != null) {
////            for (Fragment f : fragments) {
////                if (f.isVisible()) {
////                    fragment = f;
////                    break;
////                }
////            }
////        }
////        if (fragment != null
////                && fragment instanceof BackButtonListener
////                && ((BackButtonListener) fragment).onBackPressed()) {
////            return true;
////        } else {
////            return false;
////        }
//        return false;
//    }


//    @Override
//    protected String getContainerName() {
//        return NewsPagerFragment.class.getCanonicalName();
//    }
//
//    @Override
//    protected Navigator getNavigator() {
//        if (navigator == null) {
//            navigator = new NewsPagerTabNavigator(getActivity(), getChildFragmentManager(), R.id.mainFragmentContainer);
//        }
//
//        return navigator;
//    }
}
