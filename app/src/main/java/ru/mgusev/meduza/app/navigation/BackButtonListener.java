package ru.mgusev.meduza.app.navigation;

public interface BackButtonListener {

    boolean onBackPressed();

}
