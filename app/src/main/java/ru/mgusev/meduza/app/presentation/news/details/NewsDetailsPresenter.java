package ru.mgusev.meduza.app.presentation.news.details;

import java.util.List;

import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.subscribers.DisposableSubscriber;
import moxy.InjectViewState;
import ru.mgusev.meduza.app.presentation.base.BasePresenter;
import ru.mgusev.meduza.app.presentation.base.ResourceManager;
import ru.mgusev.meduza.domain.dto.news.AudioState;
import ru.mgusev.meduza.domain.dto.news.NewsItem;
import ru.mgusev.meduza.domain.dto.news.details.NewsDetailsItem;
import ru.mgusev.meduza.domain.interactor._base.BaseUseCaseParams;
import ru.mgusev.meduza.domain.interactor.audio_player.AudioPlayerProgressChangeUseCase;
import ru.mgusev.meduza.domain.interactor.audio_player.AudioPlayerStateChangeUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.DeleteBookmarkParams;
import ru.mgusev.meduza.domain.interactor.bookmark.DeleteBookmarkUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.GetBookmarkByUrlParams;
import ru.mgusev.meduza.domain.interactor.bookmark.GetBookmarkByUrlUseCase;
import ru.mgusev.meduza.domain.interactor.news.GetNewsDetailsParams;
import ru.mgusev.meduza.domain.interactor.news.GetNewsDetailsUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.InsertBookmarkParams;
import ru.mgusev.meduza.domain.interactor.bookmark.InsertBookmarkUseCase;
import ru.terrakok.cicerone.Router;
import timber.log.Timber;

@InjectViewState
public class NewsDetailsPresenter extends BasePresenter<NewsDetailsView> {

    private final GetNewsDetailsUseCase getNewsDetailsUseCase;
    private final GetBookmarkByUrlUseCase getBookmarkByUrlUseCase;
    private final InsertBookmarkUseCase insertBookmarkUseCase;
    private final DeleteBookmarkUseCase deleteBookmarkUseCase;
    private final AudioPlayerStateChangeUseCase audioPlayerStateChangeUseCase;
    private final AudioPlayerProgressChangeUseCase audioPlayerProgressChangeUseCase;

    private String url;
    private NewsDetailsItem newsDetailsItem;
    private NewsItem bookmark;
    private boolean userTrackingTouch;

    public NewsDetailsPresenter(Router router,
                                ResourceManager resourceManager,
                                String url,
                                GetNewsDetailsUseCase getNewsDetailsUseCase,
                                GetBookmarkByUrlUseCase getBookmarkByUrlUseCase,
                                InsertBookmarkUseCase insertBookmarkUseCase,
                                DeleteBookmarkUseCase deleteBookmarkUseCase,
                                AudioPlayerStateChangeUseCase audioPlayerStateChangeUseCase,
                                AudioPlayerProgressChangeUseCase audioPlayerProgressChangeUseCase) {

        super(router, resourceManager);

        this.url = url;
        this.getNewsDetailsUseCase = getNewsDetailsUseCase;
        this.getBookmarkByUrlUseCase = getBookmarkByUrlUseCase;
        this.insertBookmarkUseCase = insertBookmarkUseCase;
        this.deleteBookmarkUseCase = deleteBookmarkUseCase;
        this.audioPlayerStateChangeUseCase = audioPlayerStateChangeUseCase;
        this.audioPlayerProgressChangeUseCase = audioPlayerProgressChangeUseCase;

        bookmark = new NewsItem();

        addUseCase(this.getNewsDetailsUseCase);
        addUseCase(this.getBookmarkByUrlUseCase);
        addUseCase(this.insertBookmarkUseCase);
        addUseCase(this.deleteBookmarkUseCase);
        addUseCase(this.audioPlayerStateChangeUseCase);
        addUseCase(this.audioPlayerProgressChangeUseCase);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadData();
    }

    public void loadData() {
        getViewState().showLoader();
        getNewsDetailsUseCase.execute(new DisposableSingleObserver<NewsDetailsItem>() {
            @Override
            public void onSuccess(NewsDetailsItem item) {

                newsDetailsItem = item;
                initBookmarkItem();
                initBookmarkSubscribe();
                initAudioPlayerStateSubscribe();
                initAudioPlayerProgressSubscribe();
                getViewState().setTitle(newsDetailsItem.getTitle());
                getViewState().setSource(newsDetailsItem.getSource() == null ? null : newsDetailsItem.getSource().getName());
                getViewState().setTag(newsDetailsItem.getTag() == null ? null : newsDetailsItem.getTag().getName());
                getViewState().setDateTime(newsDetailsItem.getDateTime());
                getViewState().setNewsDetailsItem(newsDetailsItem.getContent().getBody());
                getViewState().setVisibilityAudioPlayer(newsDetailsItem.getAudio() != null);
            }

            @Override
            public void onError(Throwable e) {
                Timber.d(e);
                getViewState().showError();
            }
        }, new GetNewsDetailsParams(url));
    }

    private void initBookmarkSubscribe() {
        getBookmarkByUrlUseCase.execute(new DisposableSubscriber<List<NewsItem>>() {
            @Override
            public void onNext(List<NewsItem> bookmarks) {
                newsDetailsItem.setBookmark(!bookmarks.isEmpty());
                bookmark.setBookmark(bookmarks.isEmpty());
                getViewState().changeBookmarkIcon(newsDetailsItem.isBookmark());
            }

            @Override
            public void onError(Throwable t) {
                Timber.e(t);
            }

            @Override
            public void onComplete() {

            }
        },  new GetBookmarkByUrlParams(bookmark));
    }

    private void initBookmarkItem() {
        bookmark.setUrl(newsDetailsItem.getUrl());
        bookmark.setVersion(newsDetailsItem.getVersion());
        bookmark.setTitle(newsDetailsItem.getTitle());
        bookmark.setDateTime(newsDetailsItem.getDateTime());
        bookmark.setSecondTitle(newsDetailsItem.getSecondTitle());
        bookmark.setTag(newsDetailsItem.getTag());
        bookmark.setImage(newsDetailsItem.getImage());
        bookmark.setBookmark(newsDetailsItem.isBookmark());
    }

    public void onBookmarkIconClicked() {
        if (newsDetailsItem.isBookmark()) {
            bookmark.setBookmark(false);
            deleteBookmarkUseCase.execute(new DeleteBookmarkParams(bookmark));
        } else {
            bookmark.setBookmark(true);
            insertBookmarkUseCase.execute(new InsertBookmarkParams(bookmark));
        }
    }

    private void initAudioPlayerStateSubscribe() {
        audioPlayerStateChangeUseCase.execute(new DisposableObserver<AudioState>() {

            @Override
            public void onNext(AudioState state) {
                if (newsDetailsItem != null && newsDetailsItem.getAudio() != null) {
                    if (state.getAudio().equals(newsDetailsItem.getAudio())) {
                        getViewState().changeAudioControlButtonIcon(state.isPlaying());
                    } else {
                        getViewState().changeAudioControlButtonIcon(false);
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                Timber.d(e);
            }

            @Override
            public void onComplete() {
            }
        }, new BaseUseCaseParams());
    }

    private void initAudioPlayerProgressSubscribe() {
        audioPlayerProgressChangeUseCase.execute(new DisposableObserver<AudioState>() {

            @Override
            public void onNext(AudioState state) {
                if (newsDetailsItem != null && newsDetailsItem.getAudio() != null && !userTrackingTouch && state.getAudio().equals(newsDetailsItem.getAudio())) {
                    Timber.d(NewsDetailsPresenter.this + " " + state.getAudio().equals(newsDetailsItem.getAudio()) + " " + state.getTotalDuration() + " " + state.getCurrentPosition());
                    if (state.getTotalDuration() != -1 && state.getCurrentPosition() != -1) {
                        getViewState().updateDuration((int)(state.getTotalDuration() / 1000), (int)(state.getCurrentPosition() / 1000));
                    } else {
                        getViewState().updateDuration((int) newsDetailsItem.getAudio().getDuration(), 0);
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                Timber.d(e);
            }

            @Override
            public void onComplete() {
            }
        }, new BaseUseCaseParams());
    }

    public void onUserTrackingTouch(boolean touch) {
        userTrackingTouch = touch;
    }

    public void onBackPressed() {
        router.exit();
    }

    public void onAudioControlButtonClicked() {
        getViewState().updateAudioPlayerState(newsDetailsItem.getAudio());
    }

    public void contentLoaded() {
        getViewState().showContent();
    }

    public void contentLoadingError() {
        getViewState().showError();
    }
}