package ru.mgusev.meduza.app.presentation.news.news_list;

import java.util.List;

import moxy.viewstate.strategy.AddToEndSingleStrategy;
import moxy.viewstate.strategy.AddToEndSingleTagStrategy;
import moxy.viewstate.strategy.OneExecutionStateStrategy;
import moxy.viewstate.strategy.StateStrategyType;
import ru.mgusev.meduza.app.presentation.base.BaseView;
import ru.mgusev.meduza.domain.dto.news.Audio;
import ru.mgusev.meduza.domain.dto.news.NewsItem;

public interface NewsListView extends BaseView {
    @StateStrategyType(AddToEndSingleStrategy.class)
    void showNewsList(List<NewsItem> newsItemList);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void invalidateItem(NewsItem newsItem);

    @StateStrategyType(value = AddToEndSingleTagStrategy.class, tag = "loadMore")
    void enableLoadMore();

    @StateStrategyType(value = AddToEndSingleTagStrategy.class, tag = "loadMore")
    void disableLoadMore();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void onRefreshAdapter();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void updateAudioPlayerState(Audio audio);
}
