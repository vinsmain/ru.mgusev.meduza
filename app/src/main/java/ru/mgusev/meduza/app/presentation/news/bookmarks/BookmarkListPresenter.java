package ru.mgusev.meduza.app.presentation.news.bookmarks;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.DisposableObserver;
import io.reactivex.subscribers.DisposableSubscriber;
import moxy.InjectViewState;
import ru.mgusev.meduza.app.navigation.Screens;
import ru.mgusev.meduza.app.presentation.base.BasePresenter;
import ru.mgusev.meduza.app.presentation.base.ResourceManager;
import ru.mgusev.meduza.domain.dto.news.AudioState;
import ru.mgusev.meduza.domain.dto.news.NewsItem;
import ru.mgusev.meduza.domain.interactor._base.BaseUseCaseParams;
import ru.mgusev.meduza.domain.interactor.audio_player.AudioPlayerStateChangeUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.DeleteBookmarkParams;
import ru.mgusev.meduza.domain.interactor.bookmark.DeleteBookmarkUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.GetBookmarkListUseCase;
import ru.terrakok.cicerone.Router;
import timber.log.Timber;

@InjectViewState
public class BookmarkListPresenter extends BasePresenter<BookmarkListView> {

    private final GetBookmarkListUseCase getBookmarkListUseCase;
    private final DeleteBookmarkUseCase deleteBookmarkUseCase;
    private final AudioPlayerStateChangeUseCase audioPlayerStateChangeUseCase;

    private List<NewsItem> newsItemList;

    public BookmarkListPresenter(Router router,
                                 ResourceManager resourceManager,
                                 GetBookmarkListUseCase getBookmarkListUseCase,
                                 DeleteBookmarkUseCase deleteBookmarkUseCase,
                                 AudioPlayerStateChangeUseCase audioPlayerStateChangeUseCase) {

        super(router, resourceManager);

        this.getBookmarkListUseCase = getBookmarkListUseCase;
        this.deleteBookmarkUseCase = deleteBookmarkUseCase;
        this.audioPlayerStateChangeUseCase = audioPlayerStateChangeUseCase;

        this.newsItemList = new ArrayList<>();

        addUseCase(this.getBookmarkListUseCase);
        addUseCase(this.deleteBookmarkUseCase);
        addUseCase(this.audioPlayerStateChangeUseCase);
    }

    @Override
    protected void onFirstViewAttach() {
        loadData();
    }

    public void loadData() {
        getBookmarkListUseCase.execute(new DisposableSubscriber<List<NewsItem>>() {
            @Override
            public void onNext(List<NewsItem> bookmarkList) {
                newsItemList.clear();
                newsItemList.addAll(bookmarkList);
                getViewState().showNewsList(newsItemList);
                initAudioPlayerStateSubscribe();
            }

            @Override
            public void onError(Throwable t) {
                Timber.d(t);
                getViewState().showNewsList(null);
            }

            @Override
            public void onComplete() {
                Timber.d("Complete");
            }
        });
    }

    private void initAudioPlayerStateSubscribe() {
        audioPlayerStateChangeUseCase.execute(new DisposableObserver<AudioState>() {

            @Override
            public void onNext(AudioState state) {
                for (NewsItem item : newsItemList) {
                    if (item.getUrl().equals(state.getAudio().getUrl())) {
                        item.setPlay(state.isPlaying());
                    } else {
                        item.setPlay(false);
                    }
                }
                getViewState().showNewsList(newsItemList);
            }

            @Override
            public void onError(Throwable e) {
                Timber.d(e);
            }

            @Override
            public void onComplete() {
            }
        }, new BaseUseCaseParams());
    }

    public void onRefresh() {
        newsItemList.clear();
        getViewState().onRefreshAdapter();
        loadData();
    }

    public void onItemClicked(NewsItem newsItem) {
        router.navigateTo(new Screens.NewsDetailsScreen(newsItem.getUrl()));
    }

    public void onBookmarkIconClicked(NewsItem newsItem) {
        deleteBookmarkUseCase.execute(new DeleteBookmarkParams(newsItem));
    }

    public void onAudioControlButtonClicked(NewsItem newsItem) {
        getViewState().updateAudioPlayerState(newsItem.getAudio());
    }
}