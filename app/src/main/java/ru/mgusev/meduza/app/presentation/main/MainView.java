package ru.mgusev.meduza.app.presentation.main;

import moxy.viewstate.strategy.SkipStrategy;
import moxy.viewstate.strategy.StateStrategyType;
import ru.mgusev.meduza.app.presentation.base.BaseView;

public interface MainView extends BaseView {

    @StateStrategyType(SkipStrategy.class)
    void setNavigationItemSelected(int position);
}