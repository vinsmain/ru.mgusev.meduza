package ru.mgusev.meduza.app.ui;

import androidx.fragment.app.Fragment;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import moxy.presenter.InjectPresenter;
import moxy.presenter.ProvidePresenter;
import ru.mgusev.meduza.R;
import ru.mgusev.meduza.app.navigation.ApplicationNavigator;
import ru.mgusev.meduza.app.navigation.BackButtonListener;
import ru.mgusev.meduza.app.navigation.RouterProvider;
import ru.mgusev.meduza.app.presentation.ApplicationPresenter;
import ru.mgusev.meduza.app.presentation.base.ResourceManager;
import ru.mgusev.meduza.app.ui._base.BaseActivity;
import ru.mgusev.meduza.app.ui._base.Layout;
import ru.mgusev.meduza.app.ui._base.audio_player.AudioPlayerService;
import ru.mgusev.meduza.domain.dto.news.Audio;
import ru.mgusev.meduza.domain.dto.news.AudioState;
import ru.mgusev.meduza.domain.interactor._base.BaseUseCaseParams;
import ru.mgusev.meduza.domain.interactor.audio_player.AudioPlayerProgressChangeUseCase;
import ru.mgusev.meduza.domain.interactor.audio_player.AudioPlayerStateChangeUseCase;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import timber.log.Timber;

@Layout(id = R.layout.activity_application)
public class ApplicationActivity extends BaseActivity implements RouterProvider {

    @Inject
    NavigatorHolder holder;
    @Inject
    Router router;
    @Inject
    ResourceManager resourceManager;
    @Inject
    AudioPlayerStateChangeUseCase audioPlayerStateChangeUseCase;
    @Inject
    AudioPlayerProgressChangeUseCase audioPlayerProgressChangeUseCase;

    private Navigator navigator;

    private AudioPlayerService audioPlayerService;
    private AudioPlayerService.PlayerServiceBinder playerServiceBinder;
    private MediaControllerCompat mediaController;
    private MediaControllerCompat.Callback callback;
    private ServiceConnection serviceConnection;

    @InjectPresenter
    ApplicationPresenter presenter;
    private Disposable audioPlayerProgressDisposable;

    @ProvidePresenter
    ApplicationPresenter providePresenter() {
        return new ApplicationPresenter(router, resourceManager, getIntent());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        navigator = new ApplicationNavigator(this, getSupportFragmentManager(), R.id.fragmentContainer);
        super.onCreate(savedInstanceState);

        audioPlayerStateChangeUseCase.execute(new DisposableObserver<AudioState>() {

            @Override
            public void onNext(AudioState state) {
            }

            @Override
            public void onError(Throwable e) {
                Timber.d(e);
            }

            @Override
            public void onComplete() {
            }
        }, new BaseUseCaseParams());

        audioPlayerProgressChangeUseCase.execute(new DisposableObserver<AudioState>() {

            @Override
            public void onNext(AudioState state) {
            }

            @Override
            public void onError(Throwable e) {
                Timber.d(e);
            }

            @Override
            public void onComplete() {
            }
        }, new BaseUseCaseParams());


        callback = new MediaControllerCompat.Callback() {
            @Override
            public void onPlaybackStateChanged(PlaybackStateCompat state) {
                if (state == null)
                    return;
                boolean playing = state.getState() == PlaybackStateCompat.STATE_PLAYING;
                boolean stopped = state.getState() != PlaybackStateCompat.STATE_STOPPED && state.getState() != PlaybackStateCompat.STATE_PAUSED;
                Timber.d(String.valueOf(state.getState()));
                audioPlayerStateChangeUseCase.publish(new AudioState(audioPlayerService.getCurrentTrack(), playing, audioPlayerService.getTotalDuration(), audioPlayerService.getCurrentPosition()));
                updateProgress(playing, stopped);
            }
        };

        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                playerServiceBinder = (AudioPlayerService.PlayerServiceBinder) service;
                audioPlayerService = playerServiceBinder.getService();
                try {
                    mediaController = new MediaControllerCompat(ApplicationActivity.this, playerServiceBinder.getMediaSessionToken());
                    mediaController.registerCallback(callback);
                    callback.onPlaybackStateChanged(mediaController.getPlaybackState());
                } catch (RemoteException e) {
                    e.printStackTrace();
                    mediaController = null;
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                playerServiceBinder = null;
                if (mediaController != null) {
                    mediaController.unregisterCallback(callback);
                    mediaController = null;
                }
            }
        };

        Intent intent = new Intent(this, AudioPlayerService.class);
        Objects.requireNonNull(this).bindService(intent, serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        holder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        holder.removeNavigator();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (!(fragment instanceof BackButtonListener) || !((BackButtonListener) fragment).onBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        playerServiceBinder = null;
        if (mediaController != null) {
            mediaController.unregisterCallback(callback);
            mediaController = null;
        }
        Objects.requireNonNull(this).unbindService(serviceConnection);
    }

    @Override
    public Router getRouter() {
        return router;
    }

    public void updateAudioPlayerState(Audio audio) {
        Timber.d(String.valueOf(audio));
        audioPlayerService.updateState(audio);
    }

    public void audioPlayerSeekTo(long position) {
        audioPlayerService.setCurrentPosition(position);
    }

    private void updateProgress(boolean playing, boolean stopped) {
        if (playerServiceBinder != null) {
            if (playing) {
                audioPlayerProgressDisposable = Observable
                        .interval(500, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(p -> new AudioState(audioPlayerService.getCurrentTrack(), true, audioPlayerService.getTotalDuration(), audioPlayerService.getCurrentPosition()))
                        .subscribeOn(Schedulers.io())
                        .subscribe(state -> audioPlayerProgressChangeUseCase.publish(state), Timber::e);
            } else {
                if (audioPlayerProgressDisposable != null) audioPlayerProgressDisposable.dispose();
                if (stopped) {
                    audioPlayerSeekTo(0L);
                    audioPlayerProgressChangeUseCase.publish(new AudioState(audioPlayerService.getCurrentTrack(), true, audioPlayerService.getTotalDuration(), 0L));
                }
            }
        }
    }
}