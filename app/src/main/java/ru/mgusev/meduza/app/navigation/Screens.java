package ru.mgusev.meduza.app.navigation;

import androidx.fragment.app.Fragment;

import ru.mgusev.meduza.app.ui.main.MainFragment;
import ru.mgusev.meduza.app.ui.news.bookmarks.BookmarkListFragment;
import ru.mgusev.meduza.app.ui.news.details.NewsDetailsFragment;
import ru.mgusev.meduza.app.ui.news.news_list.NewsListFragment;
import ru.mgusev.meduza.app.ui.news.pager.NewsPagerFragment;
import ru.mgusev.meduza.app.ui.news.podcasts.PodcastListFragment;
import ru.mgusev.meduza.domain.dto.news.Category;
import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class Screens {

    public static final class MainScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return MainFragment.getInstance();
        }
    }

    public static final class TabScreen extends SupportAppScreen {

        private final int position;
        private final Category podcastsCategory;

        public TabScreen(int position, Category podcastsCategory) {
            this.position = position;
            this.podcastsCategory = podcastsCategory;
        }

        @Override
        public Fragment getFragment() {
            switch (position) {
                case MainFragment.TAB_NEWS:
                    return NewsPagerFragment.getInstance();
                case MainFragment.TAB_PODCASTS:
                    return PodcastListFragment.getInstance(podcastsCategory);
                case MainFragment.TAB_BOOKMARKS:
                    return BookmarkListFragment.getInstance();
                default:
                    return new NewsListFragment();
            }
        }
    }

    public static final class NewsDetailsScreen extends SupportAppScreen {

        private final String url;

        public NewsDetailsScreen(String url) {
            this.url = url;
        }

        @Override
        public Fragment getFragment() {
            return NewsDetailsFragment.getInstance(url);
        }
    }
}
