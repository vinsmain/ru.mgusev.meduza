package ru.mgusev.meduza.app.navigation;

import ru.terrakok.cicerone.Router;

public interface RouterProvider {

    Router getRouter();
}