package ru.mgusev.meduza.app.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.mgusev.meduza.app.ui.main.MainFragment;
import ru.mgusev.meduza.app.ui.news.bookmarks.BookmarkListFragment;
import ru.mgusev.meduza.app.ui.news.details.NewsDetailsFragment;
import ru.mgusev.meduza.app.ui.news.news_list.NewsListFragment;
import ru.mgusev.meduza.app.ui.news.pager.NewsPagerFragment;
import ru.mgusev.meduza.app.ui.news.podcasts.PodcastListFragment;

@Module
public abstract class ApplicationFragmentBindingModule {

    @ContributesAndroidInjector
    abstract MainFragment mainFragment();

    @ContributesAndroidInjector
    abstract NewsListFragment newsListFragment();

    @ContributesAndroidInjector
    abstract NewsPagerFragment newsPagerFragment();

    @ContributesAndroidInjector
    abstract PodcastListFragment podcastListFragment();

    @ContributesAndroidInjector
    abstract BookmarkListFragment bookmarkListFragment();

    @ContributesAndroidInjector
    abstract NewsDetailsFragment newsDetailsFragment();
}
