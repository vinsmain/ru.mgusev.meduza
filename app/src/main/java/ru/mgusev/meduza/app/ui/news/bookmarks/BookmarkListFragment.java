package ru.mgusev.meduza.app.ui.news.bookmarks;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import moxy.presenter.InjectPresenter;
import moxy.presenter.ProvidePresenter;
import ru.mgusev.meduza.R;
import ru.mgusev.meduza.app.navigation.RouterProvider;
import ru.mgusev.meduza.app.presentation.base.ResourceManager;
import ru.mgusev.meduza.app.presentation.news.bookmarks.BookmarkListPresenter;
import ru.mgusev.meduza.app.presentation.news.bookmarks.BookmarkListView;
import ru.mgusev.meduza.app.ui.ApplicationActivity;
import ru.mgusev.meduza.app.ui._base.BaseFragment;
import ru.mgusev.meduza.app.ui._base.Layout;
import ru.mgusev.meduza.app.ui._base.recycler.EndlessRecyclerViewScrollListener;
import ru.mgusev.meduza.app.ui.news.list_adapter.NewsAdapter;
import ru.mgusev.meduza.domain.dto.news.Audio;
import ru.mgusev.meduza.domain.dto.news.NewsItem;
import ru.mgusev.meduza.domain.interactor.audio_player.AudioPlayerStateChangeUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.DeleteBookmarkUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.GetBookmarkListUseCase;
import timber.log.Timber;

@Layout(id = R.layout.fragment_list)
public class BookmarkListFragment extends BaseFragment implements BookmarkListView {

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Inject
    ResourceManager resourceManager;
    @Inject
    GetBookmarkListUseCase getBookmarkListUseCase;
    @Inject
    DeleteBookmarkUseCase deleteBookmarkUseCase;
    @Inject
    AudioPlayerStateChangeUseCase audioPlayerStateChangeUseCase;

    @InjectPresenter
    BookmarkListPresenter presenter;

    @ProvidePresenter
    BookmarkListPresenter providePresenter() {
        return new BookmarkListPresenter(
                ((RouterProvider) getActivity()).getRouter(),
                resourceManager,
                getBookmarkListUseCase,
                deleteBookmarkUseCase,
                audioPlayerStateChangeUseCase
        );
    }

    public static BookmarkListFragment getInstance() {
        Bundle args = new Bundle();

        BookmarkListFragment instance = new BookmarkListFragment();
        instance.setArguments(args);
        return instance;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.onRefresh());
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1));

        recyclerView.setAdapter(new NewsAdapter(
                getString(R.string.error_connection_timeout),
                getString(R.string.adapter_base_text_no_data)));
        ((NewsAdapter) recyclerView.getAdapter()).setItemClick(this::onItemClick);
        ((NewsAdapter) recyclerView.getAdapter()).setBookmarkIconClick(this::onBookmarkIconClick);
        ((NewsAdapter) recyclerView.getAdapter()).setPlayerControlBtnClick(this::onPlayerControlBtnClick);
        ((NewsAdapter) recyclerView.getAdapter()).setErrorListener(() -> presenter.loadData());
    }

    @Override
    public void showNewsList(List<NewsItem> newsList) {
        ((NewsAdapter) recyclerView.getAdapter()).setItems(newsList);
    }

    @Override
    public void invalidateItem(NewsItem newsItem) {
        ((NewsAdapter) recyclerView.getAdapter()).invalidateItem(newsItem);
    }

    @Override
    public void enableLoadMore() {
    }

    @Override
    public void disableLoadMore() {
    }

    @Override
    public void onRefreshAdapter() {
        disableLoadMore();
        ((NewsAdapter) recyclerView.getAdapter()).refresh();
        swipeRefreshLayout.setRefreshing(false);
    }

    private void onItemClick(NewsItem newsItem) {
        presenter.onItemClicked(newsItem);
    }

    private void onBookmarkIconClick(NewsItem newsItem) {
        presenter.onBookmarkIconClicked(newsItem);
    }

    private void onPlayerControlBtnClick(NewsItem newsItem) {
        presenter.onAudioControlButtonClicked(newsItem);
    }

    @Override
    public void updateAudioPlayerState(Audio audio){
        ApplicationActivity activity = ((ApplicationActivity) getActivity());
        if (activity != null) {
            activity.updateAudioPlayerState(audio);
        }
    }
}