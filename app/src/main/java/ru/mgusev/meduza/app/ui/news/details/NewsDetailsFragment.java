package ru.mgusev.meduza.app.ui.news.details;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;

import com.github.anrwatchdog.ANRWatchDog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import moxy.presenter.InjectPresenter;
import moxy.presenter.ProvidePresenter;
import ru.mgusev.meduza.BuildConfig;
import ru.mgusev.meduza.R;
import ru.mgusev.meduza.app.navigation.BackButtonListener;
import ru.mgusev.meduza.app.presentation.base.ResourceManager;
import ru.mgusev.meduza.app.presentation.news.details.NewsDetailsPresenter;
import ru.mgusev.meduza.app.presentation.news.details.NewsDetailsView;
import ru.mgusev.meduza.app.ui.ApplicationActivity;
import ru.mgusev.meduza.app.ui._base.Layout;
import ru.mgusev.meduza.app.ui._base.OptionMenuSupportBaseFragment;
import ru.mgusev.meduza.app.ui._base.formatter.DateTimeFormat;
import ru.mgusev.meduza.domain.dto.news.Audio;
import ru.mgusev.meduza.domain.interactor.audio_player.AudioPlayerProgressChangeUseCase;
import ru.mgusev.meduza.domain.interactor.audio_player.AudioPlayerStateChangeUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.DeleteBookmarkUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.GetBookmarkByUrlUseCase;
import ru.mgusev.meduza.domain.interactor.news.GetNewsDetailsUseCase;
import ru.mgusev.meduza.domain.interactor.bookmark.InsertBookmarkUseCase;
import ru.terrakok.cicerone.Router;
import timber.log.Timber;

@Layout(id = R.layout.fragment_news_details)
public class NewsDetailsFragment extends OptionMenuSupportBaseFragment implements NewsDetailsView, BackButtonListener, AppCompatSeekBar.OnSeekBarChangeListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.textNewsTitle)
    AppCompatTextView textNewsTitle;
    @BindView(R.id.textNewsTag)
    AppCompatTextView textNewsTag;
    @BindView(R.id.textNewsSource)
    AppCompatTextView textNewsSource;
    @BindView(R.id.textNewsDateTime)
    AppCompatTextView textNewsDateTime;
    @BindView(R.id.audioPlayerContainer)
    LinearLayoutCompat audioPlayerContainer;
    @BindView(R.id.audioProgressBar)
    AppCompatSeekBar audioProgressBar;
    @BindView(R.id.textAudioCurrentDuration)
    AppCompatTextView audioCurrentDuration;
    @BindView(R.id.textAudioTotalDuration)
    AppCompatTextView audioTotalDuration;
    @BindView(R.id.btnAudioControl)
    FloatingActionButton audioControlButton;
    @BindView(R.id.textContentBody)
    WebView contentBody;
    @BindView(R.id.contentContainer)
    NestedScrollView contentContainer;
    @BindView(R.id.viewNewsDetailsLoader)
    View viewNewsDetailsLoader;
    @BindView(R.id.viewNewsDetailsError)
    View viewNewsDetailsError;
    @BindView(R.id.textError)
    AppCompatTextView textError;

    @Inject
    Router router;
    @Inject
    ResourceManager resourceManager;
    @Inject
    GetNewsDetailsUseCase getNewsDetailsUseCase;
    @Inject
    GetBookmarkByUrlUseCase getBookmarkByUrlUseCase;
    @Inject
    InsertBookmarkUseCase insertBookmarkUseCase;
    @Inject
    DeleteBookmarkUseCase deleteBookmarkUseCase;
    @Inject
    AudioPlayerStateChangeUseCase audioPlayerStateChangeUseCase;
    @Inject
    AudioPlayerProgressChangeUseCase audioPlayerProgressChangeUseCase;

    @InjectPresenter
    NewsDetailsPresenter presenter;

    @ProvidePresenter
    NewsDetailsPresenter providePresenter() {
        return new NewsDetailsPresenter(
                router,
                resourceManager,
                getUrl(),
                getNewsDetailsUseCase,
                getBookmarkByUrlUseCase,
                insertBookmarkUseCase,
                deleteBookmarkUseCase,
                audioPlayerStateChangeUseCase,
                audioPlayerProgressChangeUseCase);
    }

    private MenuItem addToBookmarks;

    public static NewsDetailsFragment getInstance(String url) {
        Bundle args = new Bundle();
        args.putSerializable("url", url);

        NewsDetailsFragment instance = new NewsDetailsFragment();
        instance.setArguments(args);
        return instance;
    }

    private String getUrl() {
        if (getArguments() != null && getArguments().containsKey("url")) {
            return (String) getArguments().getSerializable("url");
        }

        return null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        new ANRWatchDog().start();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ApplicationActivity activity = Objects.requireNonNull(((ApplicationActivity) getActivity()));
        activity.setSupportActionBar(toolbar);
        Objects.requireNonNull(activity.getSupportActionBar()).setDisplayShowTitleEnabled(false);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        audioProgressBar.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onCreateOptionsMenu(@NotNull Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.details_news_menu, menu);
        addToBookmarks = menu.getItem(0);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_to_bookmarks:
                presenter.onBookmarkIconClicked();
                return true;
            case R.id.action_share:
                shareNews();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void setNewsDetailsItem(String body) {
        if (body != null) {
            contentBody.getSettings().setJavaScriptEnabled(true);

            contentBody.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Timber.i("Processing webview url click...");
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(i);
                    return true;
                }

                public void onPageFinished(WebView view, String url) {
                    Timber.i("Finished loading URL: %s", url);
                    presenter.contentLoaded();
                }

                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    Timber.e("Error: %s", description);
                    presenter.contentLoadingError();
                }
            });

            contentBody.loadDataWithBaseURL("file:///android_asset/", body, "text/html", "UTF-8", null);
        }
    }

    @Override
    public void setTitle(String title) {
        textNewsTitle.setText(title);
    }

    @Override
    public void setSource(String source) {
        if (source != null) {
            textNewsSource.setText(String.format(getResources().getString(R.string.source), source));
            textNewsSource.setVisibility(View.VISIBLE);
        } else
            textNewsSource.setVisibility(View.GONE);
    }

    @Override
    public void setDateTime(long dateTime) {
        Timber.d(String.valueOf(dateTime));
        if (dateTime != 0) {
            Timber.d(DateTimeFormat.getDate(dateTime));
            textNewsDateTime.setText(DateTimeFormat.getDate(dateTime));
            textNewsDateTime.setVisibility(View.VISIBLE);
        } else
            textNewsDateTime.setVisibility(View.GONE);
    }

    @Override
    public void setTag(String tag) {
        if (tag != null) {
            textNewsTag.setText(tag.toUpperCase());
            textNewsTag.setVisibility(View.VISIBLE);
        } else
            textNewsTag.setVisibility(View.GONE);
    }

    @Override
    public void showContent() {
        if (addToBookmarks != null) addToBookmarks.setVisible(true);
        viewNewsDetailsLoader.setVisibility(View.GONE);
        viewNewsDetailsError.setVisibility(View.GONE);
        contentContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoader() {
        if (addToBookmarks != null) addToBookmarks.setVisible(false);
        viewNewsDetailsLoader.setVisibility(View.VISIBLE);
        contentContainer.setVisibility(View.GONE);
        viewNewsDetailsError.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        if (addToBookmarks != null) addToBookmarks.setVisible(false);
        viewNewsDetailsLoader.setVisibility(View.GONE);
        contentContainer.setVisibility(View.GONE);
        textError.setText(R.string.error_connection_timeout);
        viewNewsDetailsError.setVisibility(View.VISIBLE);
    }

    @OnClick({R.id.btnAudioControl, R.id.buttonRetry})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAudioControl:
                presenter.onAudioControlButtonClicked();
                break;
            case R.id.buttonRetry:
                presenter.loadData();
                break;
        }
    }

    @Override
    public void updateAudioPlayerState(Audio audio) {
        if (audio != null) {
            ApplicationActivity activity = ((ApplicationActivity) getActivity());
            if (activity != null) {
                activity.updateAudioPlayerState(audio);
            }
        }
    }

    @Override
    public void setVisibilityAudioPlayer(boolean visible) {
        if (visible) {
            audioPlayerContainer.setVisibility(View.VISIBLE);
        } else {
            audioPlayerContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateDuration(int total, int current) {
        audioTotalDuration.setText(DateTimeFormat.getDuration(total));
        audioCurrentDuration.setText(DateTimeFormat.getDuration(current));
        audioProgressBar.setMax(total);
        audioProgressBar.setProgress(current);
    }

    @Override
    public void changeAudioControlButtonIcon(boolean playing) {
        Timber.d(String.valueOf(playing));
        if (playing)
            audioControlButton.setImageResource(R.drawable.pause);
        else
            audioControlButton.setImageResource(R.drawable.play);
    }

    @Override
    public void changeBookmarkIcon(boolean bookmark) {
        if (addToBookmarks != null && bookmark) {
            addToBookmarks.setIcon(getResources().getDrawable(R.drawable.bookmark_fill));
        } else if (addToBookmarks != null) {
            addToBookmarks.setIcon(getResources().getDrawable(R.drawable.bookmark));
        }
    }

    private void shareNews() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, BuildConfig.SiteUrl + "/" + getUrl());
        sendIntent.setType("text/plain");

        Intent shareIntent = Intent.createChooser(sendIntent, null);
        startActivity(shareIntent);
    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        if (b) {
            audioCurrentDuration.setText(DateTimeFormat.getDuration(i));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        presenter.onUserTrackingTouch(true);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        ApplicationActivity activity = ((ApplicationActivity) getActivity());
        if (activity != null) {
            activity.audioPlayerSeekTo(seekBar.getProgress() * 1000);
        }
        presenter.onUserTrackingTouch(false);
    }
}