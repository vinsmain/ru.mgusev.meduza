package ru.mgusev.meduza.app.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.mgusev.meduza.app.navigation.LocalCiceroneHolder;

@Module
public class LocalNavigationModule {

    @Provides
    @Singleton
    LocalCiceroneHolder provideLocalCiceroneHolder() {
        return new LocalCiceroneHolder();
    }
}
