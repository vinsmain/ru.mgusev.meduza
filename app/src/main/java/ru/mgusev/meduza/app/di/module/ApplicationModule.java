package ru.mgusev.meduza.app.di.module;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.room.Room;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.Executors;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.mgusev.meduza.BuildConfig;
import ru.mgusev.meduza.data.gateway.NewsGatewayImpl;
import ru.mgusev.meduza.data.network.news.NewsApi;
import ru.mgusev.meduza.data.network.news.NewsNetwork;
import ru.mgusev.meduza.data.local.database.AppDatabase;
import ru.mgusev.meduza.domain.dto.news.AudioState;
import ru.mgusev.meduza.domain.dto.news.details.Content;
import ru.mgusev.meduza.domain.gateway.NewsGateway;
import ru.mgusev.meduza.domain.interactor._base.SchedulerProvider;
import ru.mgusev.meduza.domain.utils.ContentDeserializer;

@Module(includes = {ContextModule.class, NavigationModule.class, LocalNavigationModule.class})
public class ApplicationModule {

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .registerTypeAdapter(Content.class, new ContentDeserializer())
                .create();
    }

    @Provides
    @Singleton
    SchedulerProvider provideSchedulerProvider() {
        return new SchedulerProvider() {
            @Override
            public Scheduler io() {
                return Schedulers.io();
            }

            @Override
            public Scheduler ui() {
                return AndroidSchedulers.mainThread();
            }

            @Override
            public Scheduler computation() {
                return Schedulers.computation();
            }
        };
    }

    @Provides
    @Singleton
    SharedPreferences providePrefs(Context context) {
        return context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    NewsGateway provideNewsGateway(NewsNetwork network, AppDatabase database, @Named("AudioState") BehaviorSubject<AudioState> audioStateSubject, @Named("AudioProgress") BehaviorSubject<AudioState> audioProgressSubject) {
        return new NewsGatewayImpl(network, database, audioStateSubject, audioProgressSubject);
    }

    @Provides
    @Singleton
    @Named("AudioState")
    BehaviorSubject<AudioState> provideAudioStateSubject() {
        return BehaviorSubject.create();
    }

    @Provides
    @Singleton
    @Named("AudioProgress")
    BehaviorSubject<AudioState> provideAudioProgressSubject() {
        return BehaviorSubject.create();
    }

    @Provides
    @Singleton
    NewsApi provideNewsApi(OkHttpClient client) {
        return createService(client, NewsApi.class);
    }

    @Provides
    @Singleton
    AppDatabase provideAppDatabase(Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, "database").build();
    }

    @Provides
    @Singleton
    OkHttpClient provideLoginClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG)
            builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));

        return builder.build();
    }

    private <S> S createService(OkHttpClient client, Class<S> serviceClass) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .baseUrl(BuildConfig.SiteUrl)
                .callbackExecutor(Executors.newFixedThreadPool(3))
                .build()
                .create(serviceClass);
    }
}
