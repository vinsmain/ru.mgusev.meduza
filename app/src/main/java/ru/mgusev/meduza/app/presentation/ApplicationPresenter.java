package ru.mgusev.meduza.app.presentation;

import android.content.Intent;

import moxy.InjectViewState;

import ru.mgusev.meduza.app.navigation.Screens;
import ru.mgusev.meduza.app.presentation.base.BasePresenter;
import ru.mgusev.meduza.app.presentation.base.BaseView;
import ru.mgusev.meduza.app.presentation.base.ResourceManager;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class ApplicationPresenter extends BasePresenter<BaseView> {

    private Intent intent;

    public ApplicationPresenter(Router router, ResourceManager resourceManager, Intent intent) {
        super(router, resourceManager);
        this.intent = intent;
    }

    @Override
    protected void onFirstViewAttach() {
        final String action = intent.getAction();
        final String data = intent.getDataString();
        if (Intent.ACTION_VIEW.equals(action) && data != null) {
            final String url = data.replace("https://meduza.io/", "");
            router.replaceScreen(new Screens.NewsDetailsScreen(url));

        } else {
            router.replaceScreen(new Screens.MainScreen());
        }
    }
}
