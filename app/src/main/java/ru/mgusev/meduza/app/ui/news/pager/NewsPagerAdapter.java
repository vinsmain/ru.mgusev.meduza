package ru.mgusev.meduza.app.ui.news.pager;

import android.content.Context;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import ru.mgusev.meduza.R;
import ru.mgusev.meduza.app.ui.news.news_list.NewsListFragment;
import ru.mgusev.meduza.domain.dto.news.Category;

public class NewsPagerAdapter extends FragmentPagerAdapter {

    private String[] newsFragmentTitleArray;
    private String[] newsFragmentApiArray;

    public NewsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        newsFragmentTitleArray = context.getResources().getStringArray(R.array.titleCategoryArray);
        newsFragmentApiArray = context.getResources().getStringArray(R.array.apiCategoryArray);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        return super.instantiateItem(container, position);
    }

    @Override
    public Fragment getItem(int position) {
        return NewsListFragment.getInstance(new Category(newsFragmentApiArray[position]));
//        switch (position) {
//            case 0:
//                return BookmarkListFragment.getInstance(new Category("news"));
//            case 1:
//                return BookmarkListFragment.getInstance(new Category("podcasts"));
//            case 2:
//                return BookmarkListFragment.getInstance(new Category("favourites"));
//
//            default:
//                return BookmarkListFragment.getInstance(new Category("news"));
//        }
    }

    @Override
    public int getCount() {
        return newsFragmentApiArray.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return newsFragmentTitleArray[position];
    }
}
