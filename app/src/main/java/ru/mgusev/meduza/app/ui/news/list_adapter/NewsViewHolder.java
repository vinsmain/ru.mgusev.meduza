package ru.mgusev.meduza.app.ui.news.list_adapter;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import ru.mgusev.meduza.R;
import ru.mgusev.meduza.app.ui._base.formatter.DateTimeFormat;
import ru.mgusev.meduza.app.ui._base.recycler.adapter.BaseAdapter;
import ru.mgusev.meduza.app.ui._base.recycler.adapter.BaseViewHolder;
import ru.mgusev.meduza.app.ui.news.inteface.OnBookmarkIconClick;
import ru.mgusev.meduza.app.ui.news.inteface.OnPlayerControlBtnClick;
import ru.mgusev.meduza.domain.dto.news.Image;
import ru.mgusev.meduza.domain.dto.news.NewsItem;
import ru.mgusev.meduza.domain.dto.news.Tag;

public class NewsViewHolder extends BaseViewHolder<NewsItem> {

    @BindView(R.id.imageNewsPreview)
    AppCompatImageView imageNewsPreview;
    @BindView(R.id.textNewsTitle)
    AppCompatTextView textNewsTitle;
    @BindView(R.id.textNewsSubtitle)
    AppCompatTextView textNewsSubtitle;
    @BindView(R.id.textNewsTag)
    AppCompatTextView textNewsTag;
    @BindView(R.id.textNewsDateTime)
    AppCompatTextView textNewsDateTime;

    @BindView(R.id.viewItemClick)
    View viewItemClick;
    @BindView(R.id.btnAddToBookmarks)
    AppCompatImageButton btnBookmarkIcon;
    @BindView(R.id.btnAudioControl)
    AppCompatImageButton btnAudioControl;

    private OnBookmarkIconClick<NewsItem> bookmarkIconClick;
    private OnPlayerControlBtnClick<NewsItem> playerControlBtnClick;

    public NewsViewHolder(View itemView, BaseAdapter.OnItemClick<NewsItem> itemClick, OnBookmarkIconClick<NewsItem> bookmarkIconClick, OnPlayerControlBtnClick<NewsItem> playerControlBtnClick) {
        super(itemView, itemClick);
        this.bookmarkIconClick = bookmarkIconClick;
        this.playerControlBtnClick = playerControlBtnClick;
    }

    @Override
    public void fill(NewsItem item, boolean isLast) {
        if (itemClick != null) {
            viewItemClick.setOnClickListener(v -> itemClick.onItemClick(item));
        }

        if (bookmarkIconClick != null) {
            btnBookmarkIcon.setOnClickListener(v -> bookmarkIconClick.onBookmarkIconClick(item));
        }

        if (playerControlBtnClick != null) {
            btnAudioControl.setOnClickListener(v -> playerControlBtnClick.onPlayerControlBtnClick(item));
        }

        setTitle(item.getTitle());
        setDateTime(item.getDateTime());
        setSecondTitle(item.getSecondTitle());
        setTag(item.getTag());
        setImage(item.getImage());
        setBtnBookmarkIcon(item.isBookmark());
        setVisibilityBtnPlayer(item.getAudio() != null);
        setBtnPlayerIcon(item.isPlay());
    }

    @Override
    public void fill(NewsItem item, boolean isLast, List<Object> payloads) {
        if (payloads.isEmpty()) {
            fill(item, isLast);
        } else {
            for (int i = 0; i < payloads.size(); i++) {
                Bundle bundle = (Bundle) payloads.get(i);
                for (String key : bundle.keySet()) {
                    switch (key) {
                        case NewsItem.KEY_TITLE:
                            setTitle(item.getTitle());
                            break;
                        case NewsItem.KEY_DATE_TIME:
                            setDateTime(item.getDateTime());
                            break;
                        case NewsItem.KEY_SECOND_TITLE:
                            setSecondTitle(item.getSecondTitle());
                            break;
                        case NewsItem.KEY_TAG:
                            setTag(item.getTag());
                            break;
                        case NewsItem.KEY_IMAGE:
                            setImage(item.getImage());
                            break;
                        case NewsItem.KEY_BOOKMARK:
                            setBtnBookmarkIcon(bundle.getBoolean(key));
                            break;
                        case NewsItem.KEY_PLAY:
                            setBtnPlayerIcon(bundle.getBoolean(key));
                            break;
                    }
                }
            }
        }
    }

    @Override
    public void clear() {

    }

    private void setTitle(String title) {
        textNewsTitle.setText(title);
    }

    private void setDateTime(long dateTime) {
        textNewsDateTime.setText(DateTimeFormat.getTime(dateTime));
    }

    private void setImage(Image image) {
        if (image != null) {
            Glide.with(viewItemClick)
                    .load(Uri.parse(image.getAbsoluteImageUrl()))
                    .into(imageNewsPreview);
            imageNewsPreview.setVisibility(View.VISIBLE);
        } else
            imageNewsPreview.setVisibility(View.GONE);
    }

    private void setSecondTitle(String secondTitle) {
        if (secondTitle != null && !secondTitle.equals("")) {
            textNewsSubtitle.setVisibility(View.VISIBLE);
            textNewsSubtitle.setText(secondTitle);
        } else
            textNewsSubtitle.setVisibility(View.GONE);
    }

    private void setTag(Tag tag) {
        if (tag != null && !tag.getName().equals("")) {
            textNewsTag.setVisibility(View.VISIBLE);
            textNewsTag.setText(tag.getName().toUpperCase());
        } else
            textNewsTag.setVisibility(View.GONE);
    }

    private void setBtnBookmarkIcon(boolean bookmark) {
        if (bookmark) {
            btnBookmarkIcon.setImageResource(R.drawable.bookmark_fill);
        } else
            btnBookmarkIcon.setImageResource(R.drawable.bookmark);
    }

    private void setVisibilityBtnPlayer(boolean visible) {
        if (visible)
            btnAudioControl.setVisibility(View.VISIBLE);
        else
            btnAudioControl.setVisibility(View.GONE);
    }

    private void setBtnPlayerIcon(boolean play) {
        if (play) {
            btnAudioControl.setImageResource(R.drawable.pause);
        } else
            btnAudioControl.setImageResource(R.drawable.play);
    }
}