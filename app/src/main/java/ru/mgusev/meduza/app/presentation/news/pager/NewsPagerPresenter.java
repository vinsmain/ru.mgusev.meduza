package ru.mgusev.meduza.app.presentation.news.pager;

import moxy.InjectViewState;
import ru.mgusev.meduza.app.presentation.base.BasePresenter;
import ru.mgusev.meduza.app.presentation.base.ResourceManager;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class NewsPagerPresenter extends BasePresenter<NewsPagerView> {

    public NewsPagerPresenter(Router router, ResourceManager resourceManager) {
        super(router, resourceManager);
    }
}